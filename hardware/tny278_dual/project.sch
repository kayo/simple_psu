EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:addon
LIBS:project-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L PC817 U4
U 1 1 571D1672
P 6100 5300
F 0 "U4" H 5900 5500 50  0000 L CNN
F 1 "PC817" H 6100 5500 50  0000 L CNN
F 2 "Local:SODIP-4_W7.62mm" H 5900 5100 50  0001 L CIN
F 3 "" H 6100 5300 50  0000 L CNN
	1    6100 5300
	-1   0    0    -1  
$EndComp
$Comp
L D_Schottky D3
U 1 1 571D18D1
P 6350 2900
F 0 "D3" H 6350 3000 50  0000 C CNN
F 1 "US1D" H 6350 2800 50  0000 C CNN
F 2 "Local:SMB_Standard" H 6350 2900 60  0001 C CNN
F 3 "" H 6350 2900 60  0000 C CNN
	1    6350 2900
	-1   0    0    -1  
$EndComp
$Comp
L CP C4
U 1 1 571D1A26
P 7800 3350
F 0 "C4" H 7825 3450 50  0000 L CNN
F 1 "1000u 35V" H 7550 3250 50  0000 L CNN
F 2 "Local:C_Radial_D10_L20_P5" H 7838 3200 30  0001 C CNN
F 3 "" H 7800 3350 60  0000 C CNN
	1    7800 3350
	1    0    0    -1  
$EndComp
$Comp
L TRANS_4X T1
U 1 1 571D1D9E
P 5700 3300
F 0 "T1" H 5600 3650 50  0000 C CNN
F 1 "PC40 EE19 GAP 0.4mm" H 5900 2800 50  0000 C CNN
F 2 "Local:TRANS_EE19_5" H 5700 3300 60  0001 C CNN
F 3 "" H 5700 3300 60  0000 C CNN
	1    5700 3300
	1    0    0    -1  
$EndComp
$Comp
L C C1
U 1 1 571D205F
P 5700 6850
F 0 "C1" H 5725 6950 50  0000 L CNN
F 1 "2n2 250V" H 5550 6750 50  0000 L CNN
F 2 "Local:C_Rect_L7_W4.5_P5" H 5738 6700 30  0001 C CNN
F 3 "" H 5700 6850 60  0000 C CNN
	1    5700 6850
	1    0    0    -1  
$EndComp
$Comp
L TNY278PN U1
U 1 1 571D2546
P 4700 4100
F 0 "U1" H 4700 4450 60  0000 C CNN
F 1 "TNY278PN" H 4700 3750 60  0000 C CNN
F 2 "Local:DIP-8_n3_W7.62mm" H 4700 4100 60  0001 C CNN
F 3 "" H 4700 4100 60  0000 C CNN
	1    4700 4100
	1    0    0    -1  
$EndComp
Text GLabel 4700 4900 3    60   Input ~ 0
GNDHV
$Comp
L ZENER D2
U 1 1 571D2984
P 4350 2700
F 0 "D2" H 4350 2800 50  0000 C CNN
F 1 "P6KE150A" H 4350 2600 50  0000 C CNN
F 2 "Local:Diode_DO-41_SOD81_Vertical_KathodeUp" H 4350 2700 60  0001 C CNN
F 3 "" H 4350 2700 60  0000 C CNN
	1    4350 2700
	0    -1   -1   0   
$EndComp
Text GLabel 4700 2300 1    60   Input ~ 0
VCCHV
$Comp
L R R2
U 1 1 571D2BF6
P 4700 2650
F 0 "R2" V 4780 2650 50  0000 C CNN
F 1 "100" V 4700 2650 50  0000 C CNN
F 2 "Local:R_1206" V 4630 2650 30  0001 C CNN
F 3 "" H 4700 2650 30  0000 C CNN
	1    4700 2650
	-1   0    0    1   
$EndComp
$Comp
L C C3
U 1 1 571D2D0C
P 4700 3250
F 0 "C3" H 4725 3350 50  0000 L CNN
F 1 "10n 1kV" H 4550 3150 50  0000 L CNN
F 2 "Local:C_1206" H 4738 3100 30  0001 C CNN
F 3 "" H 4700 3250 60  0000 C CNN
	1    4700 3250
	1    0    0    -1  
$EndComp
$Comp
L R R4
U 1 1 571D2E61
P 4350 3250
F 0 "R4" V 4430 3250 50  0000 C CNN
F 1 "1K" V 4350 3250 50  0000 C CNN
F 2 "Local:R_1206" V 4280 3250 30  0001 C CNN
F 3 "" H 4350 3250 30  0000 C CNN
	1    4350 3250
	-1   0    0    1   
$EndComp
$Comp
L D D6
U 1 1 571D2F9E
P 4950 3500
F 0 "D6" H 4950 3600 50  0000 C CNN
F 1 "FR107" H 4950 3400 50  0000 C CNN
F 2 "Local:D_SOD-123" H 4950 3500 60  0001 C CNN
F 3 "" H 4950 3500 60  0000 C CNN
	1    4950 3500
	1    0    0    -1  
$EndComp
$Comp
L C C8
U 1 1 571D3A6B
P 4200 4550
F 0 "C8" H 4225 4650 50  0000 L CNN
F 1 "10u 16V" H 4050 4450 50  0000 L CNN
F 2 "Local:C_0805" H 4238 4400 30  0001 C CNN
F 3 "" H 4200 4550 60  0000 C CNN
	1    4200 4550
	1    0    0    -1  
$EndComp
$Comp
L FUSE F1
U 1 1 571D4CE6
P 2150 2300
F 0 "F1" H 2250 2350 50  0000 C CNN
F 1 "3A" H 2050 2250 50  0000 C CNN
F 2 "Local:FUSE_3A" H 2150 2300 60  0001 C CNN
F 3 "" H 2150 2300 60  0000 C CNN
	1    2150 2300
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X02 P3
U 1 1 571D520B
P 1300 2350
F 0 "P3" H 1300 2500 50  0000 C CNN
F 1 "PWR" V 1400 2350 50  0000 C CNN
F 2 "Local:2PIN_HEAD" H 1300 2350 60  0001 C CNN
F 3 "" H 1300 2350 60  0000 C CNN
	1    1300 2350
	-1   0    0    -1  
$EndComp
$Comp
L CP C2
U 1 1 571D5FC0
P 3300 2900
F 0 "C2" H 3325 3000 50  0000 L CNN
F 1 "47u 400V" H 3150 2800 50  0000 L CNN
F 2 "Local:C_Radial_D16_L25_P7.5" H 3338 2750 30  0001 C CNN
F 3 "" H 3300 2900 60  0000 C CNN
	1    3300 2900
	1    0    0    -1  
$EndComp
Text GLabel 3300 2600 1    60   Input ~ 0
VCCHV
Text GLabel 2000 2800 1    60   Input ~ 0
GNDHV
$Comp
L CONN_01X02 P4
U 1 1 571D735E
P 10800 2650
F 0 "P4" H 10800 2800 50  0000 C CNN
F 1 "12V" V 10900 2650 50  0000 C CNN
F 2 "Local:Pin_Header_Straight_1x02_Pitch2.54mm" H 10800 2650 60  0001 C CNN
F 3 "" H 10800 2650 60  0000 C CNN
	1    10800 2650
	1    0    0    1   
$EndComp
Text GLabel 6600 2900 2    60   Input ~ 0
VCC1+12V
Text GLabel 6600 4500 0    60   Input ~ 0
VCC1+12V
$Comp
L R R7
U 1 1 571E341B
P 6700 5250
F 0 "R7" V 6780 5250 50  0000 C CNN
F 1 "1K" V 6700 5250 50  0000 C CNN
F 2 "Local:R_0603" V 6630 5250 30  0001 C CNN
F 3 "" H 6700 5250 30  0000 C CNN
	1    6700 5250
	-1   0    0    -1  
$EndComp
$Comp
L R R5
U 1 1 571E3FEC
P 6700 4750
F 0 "R5" V 6780 4750 50  0000 C CNN
F 1 "680" V 6700 4750 50  0000 C CNN
F 2 "Local:R_0603" V 6630 4750 30  0001 C CNN
F 3 "" H 6700 4750 30  0000 C CNN
	1    6700 4750
	-1   0    0    1   
$EndComp
$Comp
L D_Schottky D5
U 1 1 571E5BCC
P 6350 3400
F 0 "D5" H 6350 3500 50  0000 C CNN
F 1 "US1D" H 6350 3300 50  0000 C CNN
F 2 "Local:SMB_Standard" H 6350 3400 60  0001 C CNN
F 3 "" H 6350 3400 60  0000 C CNN
	1    6350 3400
	-1   0    0    -1  
$EndComp
$Comp
L CP C5
U 1 1 571E5DB9
P 8300 3350
F 0 "C5" H 8325 3450 50  0000 L CNN
F 1 "470u 25V" H 8100 3250 50  0000 L CNN
F 2 "Local:C_Radial_D8_L11.5_P3.5" H 8338 3200 30  0001 C CNN
F 3 "" H 8300 3350 60  0000 C CNN
	1    8300 3350
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X02 P6
U 1 1 571E6C57
P 10800 3650
F 0 "P6" H 10800 3800 50  0000 C CNN
F 1 "24V" V 10900 3650 50  0000 C CNN
F 2 "Local:Pin_Header_Straight_1x02_Pitch2.54mm" H 10800 3650 60  0001 C CNN
F 3 "" H 10800 3650 60  0000 C CNN
	1    10800 3650
	1    0    0    1   
$EndComp
$Comp
L CP C6
U 1 1 571E7B19
P 8800 3350
F 0 "C6" H 8825 3450 50  0000 L CNN
F 1 "1000u 35V" H 8550 3250 50  0000 L CNN
F 2 "Local:C_Radial_D10_L20_P5" H 8838 3200 30  0001 C CNN
F 3 "" H 8800 3350 60  0000 C CNN
	1    8800 3350
	1    0    0    -1  
$EndComp
Text GLabel 6600 3400 2    60   Input ~ 0
VCC2+24V
Text Notes 700  1900 0    47   ~ 0
/* PCB fuse width selection */\n\nEQ[w]: solve(%pi * (d/2)^2 = h * w, w)[1], solve(I^2 = 80^2 * d^3, d)[3];\nEQ[w], h=0.035, I=3, numer;
$Comp
L THERMISTOR TH1
U 1 1 571F13A5
P 1700 2950
F 0 "TH1" V 1800 3000 50  0000 C CNN
F 1 "NTC 10R" V 1600 2950 50  0000 C BNN
F 2 "Local:NTC_11x5.5_f7.5" H 1700 2950 60  0001 C CNN
F 3 "" H 1700 2950 60  0000 C CNN
	1    1700 2950
	-1   0    0    1   
$EndComp
$Comp
L CONN_01X01 M2
U 1 1 5743FFEF
P 1300 2950
F 0 "M2" H 1300 3050 50  0000 C CNN
F 1 "MOUNT" V 1400 2950 50  0000 C CNN
F 2 "Local:MountingHole_3mm" H 1300 2950 60  0001 C CNN
F 3 "" H 1300 2950 60  0000 C CNN
	1    1300 2950
	0    -1   -1   0   
$EndComp
$Comp
L CONN_01X01 M1
U 1 1 5743FFFC
P 1000 2950
F 0 "M1" H 1000 3050 50  0000 C CNN
F 1 "MOUNT" V 1100 2950 50  0000 C CNN
F 2 "Local:MountingHole_3mm" H 1000 2950 60  0001 C CNN
F 3 "" H 1000 2950 60  0000 C CNN
	1    1000 2950
	0    -1   -1   0   
$EndComp
$Comp
L R R3
U 1 1 57467054
P 4100 3250
F 0 "R3" V 4180 3250 50  0000 C CNN
F 1 "2M2" V 4100 3250 50  0000 C CNN
F 2 "Local:R_1206" V 4030 3250 30  0001 C CNN
F 3 "" H 4100 3250 30  0000 C CNN
	1    4100 3250
	-1   0    0    1   
$EndComp
$Comp
L R R1
U 1 1 57467419
P 4100 2650
F 0 "R1" V 4180 2650 50  0000 C CNN
F 1 "2M2" V 4100 2650 50  0000 C CNN
F 2 "Local:R_1206" V 4030 2650 30  0001 C CNN
F 3 "" H 4100 2650 30  0000 C CNN
	1    4100 2650
	-1   0    0    1   
$EndComp
Text Notes 650  1350 0    47   ~ 0
/* Undervoltage threshold resistor selection */\n\nEQ[R[UV]]: R[UV] = U[IN,MIN] / I[UV,TH];\nEQ[I[R[UV],MAX]]: I[R[UV]] = U[IN,MAX] / R[UV];\nEQ[P[R[UV]]]: P[R[UV]] = U[IN,MAX]^2 / R[UV];\nPR[U[IN]]: [I[UV,TH]=25e-6, U[IN,MIN]=110, U[IN,MAX]=350]$\nPR[R[UV]]: EQ[R[UV]], PR[U[IN]];\nPR[I[R[UV],MAX]]: EQ[I[R[UV],MAX]], PR[U[IN]], PR[R[UV]];\nPR[P[R[UV]]]: EQ[P[R[UV]]], PR[U[IN]], PR[R[UV]];
Text Notes 3800 3000 0    60   ~ 0
R[UV]
Text GLabel 5200 3900 2    60   Input ~ 0
FB_GATE
Text GLabel 4200 3900 1    60   Input ~ 0
BP/M
Text GLabel 4100 4300 0    60   Input ~ 0
EN/UV
Text Notes 750  10150 0    60   ~ 0
/* Ferrite core selection */\n/* F - edge coefficient */\n/* A[e] - effective cross-sectional area */\n/* l[e] - effective magnetic path length */\n/* V[e] - effective core volume */\n/* delta - clearance */\n/* B[w] - width of winding */\n/* I[peak] - peak current */\n\nEQ[L]: L = N * Phi / i$\nEQ[LI[peak]^2]: L * I[peak] ^ 2 = B[max] * A[e] * l[e] / (mu[0] * mu[e]);\nEQ[F]: F = 1 + delta / sqrt(A[e]) * log(2 * B[w] / delta);\nEQ[mu[e]]: mu[e] = mu[0] * A[e] / (l[e] / mu[i] + delta / F);\nEQ[A[L]]: A[L] = mu[0] * mu[e] / (l[e] / A[e]);\n\nPR[PC40]: mu[i]=2300$\nPR[EE19]: [A[e]=23e-6, l[e]=39.4e-3]$\nPR[EE25]: [A[e]=51.8e-6, l[e]=57.8e-3]$\nPR[in]: [mu[0]=0.4e-6*%pi, L=1e-3/*1mH*/, delta=0.1e-3, B[w]=8.3e-3, B[max]=300e-3, I[peak]=2]$\nPR[F]: EQ[F], PR[in], PR[EE19], numer;\nPR[mu[e]]: EQ[mu[e]], PR[PC40], PR[EE19], PR[in], PR[F], numer;\nPR[A[L]]: EQ[A[L]], PR[in], PR[mu[e]], PR[EE19], numer;\nPR[LI[peak]^2]: EQ[LI[peak]^2], PR[in], PR[mu[e]], PR[EE19], numer;
Text Notes -3750 7450 0    60   ~ 0
/* Ferrite core selection */\n/* F - edge coefficient */\n/* A[e] - effective cross-sectional area */\n/* l[e] - effective magnetic path length */\n/* V[e] - effective core volume */\n/* delta - magnetic clearance */\n/* B[w] - width of winding */\n/* I[peak] - peak current */\n/* mu[0] - magnetic field constant */\n/* mu[e] - relative effective permeability */\n/* mu[i] - relative initial permeability */\n\nEQ[L]: L = N * Phi / i$\nEQ[LI[peak]^2]: L * I[peak] ^ 2 = B[max] * A[e] * l[e] / (mu[0] * mu[e]);\nEQ[F]: F = 1 + delta / sqrt(A[e]) * log(2 * B[w] / delta);\nEQ[mu[e]]: mu[e] = mu[0] * A[e] / (l[e] / mu[i] + delta / F);\nEQ[A[L]]: A[L] = mu[0] * mu[e] / (l[e] / A[e]);\nEQ[A[L]], EQ[mu[e]]$\n\nPR[N27]: [mu[i]=2000, B[max]=300e-3]$\nPR[PC40]: [mu[i]=2300, B[max]=300e-3]$\nPR[EE19]: [A[e]=23e-6, l[e]=39.4e-3, B[w]=8.3e-3]$\nPR[EE25]: [A[e]=51.8e-6, l[e]=57.8e-3, B[w]=8.3e-3]$\nPR[EE42]: [A[e]=240, l[e]=97, B[w]=26]$\n\nPR[in]: [mu[0]=0.4e-6*%pi, L=1e-3/*1mH*/, delta=2, I[peak]=2]$\n\n/*PR[F]: EQ[F], PR[in], PR[EE19], numer;\nPR[mu[e]]: EQ[mu[e]], PR[PC40], PR[EE19], PR[in], PR[F], numer;\nPR[A[L]]: EQ[A[L]], PR[in], PR[mu[e]], PR[EE19], numer;\nPR[LI[peak]^2]: EQ[LI[peak]^2], PR[in], PR[PC40], PR[mu[e]], PR[EE19], numer;*/\n\nPR[F]: EQ[F], PR[in], PR[EE42], numer;\nPR[mu[e]]: EQ[mu[e]], PR[N27], PR[EE42], PR[in], PR[F], numer;\nPR[A[L]]: EQ[A[L]], PR[in], PR[mu[e]], PR[EE42], numer;\nPR[LI[peak]^2]: EQ[LI[peak]^2], PR[in], PR[N27], PR[mu[e]], PR[EE42], numer;
Text Notes 7050 1400 0    60   ~ 0
/* I[peak] - peak current (A) */\n/* U[r] - reflected voltage (V) */\n/* U[dc,min] - minimum input DC voltage (V) */\nEQ[P[in,max]]: P[in,max] = P[out,max] / eta;\nEQ[D[max]]: D[max] = U[r] / (U[r] + U[dc,min]);\nEQ[I[peak]]: I[peak] = 2 * P[in,max] / (U[dc,min] * D[max]), EQ[D[max]], EQ[P[in,max]];\n\nPR[in]: [U[r]=150, U[dc,min]=90, P[out,max]=20, eta=75e-2];\nEQ[I[peak]], PR[in], numer;
Text Notes 750  7550 0    60   ~ 0
/* U[r] - reflected voltage (V) */\n/* U[dc,min] - minimum input DC voltage (V) */\n/* n - transformation ratio */\n\nEQ[P[in,max]]: P[in,max] = P[out,max] / eta;\nEQ[D[max]]: D[max] = U[r] / (U[r] + U[dc,min]);\nEQ[I[peak]]: I[peak] = 2 * P[in,max] / (U[dc,min] * D[max]);\nEQ[I[pri]]: I[pri] = P[in,max] / U[dc,max];\nEQ[L[pri,max]]: L[pri,max] = U[dc,min] * D[max] / (I[pri] * f[sw]);\nEQ[N[pri]]: N[pri] = L[pri] * I[pri] / (B[max] * A[e]);\nEQ[n]: n = U[r] / (U[out] + U[D]);\n\nPR[in]: [U[r]=150, U[dc,min]=90, U[dc,max]=320, P[out,max]=20, eta=75e-2, f[sw]=125e3]$\nPR[out]: [U[out]=12, U[D]=0.8];\nPR[out2]: [U[out]=5, U[D]=0.8];\nPR[PC40]: [mu[i]=2300, B[max]=300e-3]$\nPR[EE19]: [A[e]=23e-6, l[e]=39.4e-3]$\n\nPR[P[in,max]]: EQ[P[in,max]], PR[in], numer;\nPR[I[pri]]: EQ[I[pri]], PR[in], PR[P[in,max]];\nPR[D[max]]: EQ[D[max]], PR[in], numer;\nPR[I[peak]]: EQ[I[peak]], PR[in], PR[P[in,max]], PR[D[max]], numer;\nPR[L[pri,max]]: EQ[L[pri,max]], PR[in], PR[D[max]], PR[I[pri]], numer;\nPR[L[pri]]: L[pri]=1e-3;\nEQ[N[pri]], PR[I[pri]], PR[L[pri]], PR[PC40], PR[EE19];\nPR[N[pri]]: N[pri]=68;\nPR[n]: EQ[n], PR[in], PR[out];\nPR[n2]: EQ[n], PR[in], PR[out2];
Text Notes 7150 2000 0    60   ~ 0
EQ: N[sec] = N[pri] / n[tr];\nPR: [N[pri]=68, n[12]=7.5, n[5.3]=17, n[7.8]=11.5];\nEQ, n[tr]=n[12], N[sec]=N[12], PR;\nEQ, n[tr]=n[5.3], N[sec]=N[5.3], PR;\nEQ, n[tr]=n[7.8], N[sec]=N[7.8], PR;
Text Notes 11700 4250 0    60   ~ 0
/* Ferrite core selection */\n/* A[e] - effective cross-sectional area (m^2) */\n/* l[e] - effective magnetic path length (m) */\n/* V[e] - effective core volume (m^3) */\n/* l[g] - ferrite core gap length (m) */\n/* B[max] - magnet flux density (Gauss) */\n/* I[peak] - peak current (A) */\n/* N - number of turns */\n/* mu[0] - magnetic field constant (H/m) */\n/* mu[e] - relative effective permeability (H/m) */\n/* mu[i] - relative initial permeability (H/m) */\n\nEQ[mu[e]]: mu[e] = B[max] * l[e] / (mu[0] * N * I[peak]);\nEQ[l[g]]: l[g] = l[e] * (1/mu[e] - 1/mu[i]);\n\nPR[in]: [mu[0]=0.4e-6*%pi, L=1e-3/*1mH*/, I[peak]=1, N=62]$\nPR[N27]: [mu[i]=2000, B[max]=300e-3]$\nPR[PC40]: [mu[i]=2300, B[max]=300e-3]$\nPR[EE19]: [A[e]=23e-6, l[e]=39.4e-3]$\nPR[EE25]: [A[e]=51.8e-6, l[e]=57.8e-3]$\n\nPR[mu[e]]: EQ[mu[e]], PR[in], PR[PC40], PR[EE19], numer;\nPR[l[g]]: EQ[l[g]], PR[PC40], PR[EE19], PR[mu[e]], numer;
$Comp
L DB D1
U 1 1 58887A9A
P 2500 2900
F 0 "D1" H 2250 3200 50  0000 C CNN
F 1 "DB107S" H 2500 2900 50  0000 C CNN
F 2 "Local:DB4S" H 2500 2900 50  0001 C CNN
F 3 "" H 2500 2900 50  0000 C CNN
	1    2500 2900
	1    0    0    1   
$EndComp
Text GLabel 3300 3150 3    60   Input ~ 0
GNDHV
Text GLabel 3000 2800 1    60   Input ~ 0
VCCHV
Text GLabel 6200 3200 2    60   Input ~ 0
GND1
$Comp
L D_Schottky D4
U 1 1 5888C339
P 6650 3100
F 0 "D4" H 6650 3200 50  0000 C CNN
F 1 "US1D" H 6650 3000 50  0000 C CNN
F 2 "Local:SMB_Standard" H 6650 3100 60  0001 C CNN
F 3 "" H 6650 3100 60  0000 C CNN
	1    6650 3100
	-1   0    0    -1  
$EndComp
$Comp
L CP C9
U 1 1 5888DD6D
P 9500 5450
F 0 "C9" H 9525 5550 50  0000 L CNN
F 1 "47u 25V" H 9350 5350 50  0000 L CNN
F 2 "Local:C_Radial_D5_L11_P2.5" H 9538 5300 30  0001 C CNN
F 3 "" H 9500 5450 60  0000 C CNN
	1    9500 5450
	1    0    0    -1  
$EndComp
Text GLabel 7800 3100 1    60   Input ~ 0
VCC1+12V
Text GLabel 7800 3600 3    60   Input ~ 0
GND1
Text GLabel 6900 3100 2    60   Input ~ 0
VCC1+6V
$Comp
L D_Schottky D7
U 1 1 5889092F
P 6650 3600
F 0 "D7" H 6650 3700 50  0000 C CNN
F 1 "US1D" H 6650 3500 50  0000 C CNN
F 2 "Local:SMB_Standard" H 6650 3600 60  0001 C CNN
F 3 "" H 6650 3600 60  0000 C CNN
	1    6650 3600
	-1   0    0    -1  
$EndComp
Text GLabel 6200 3700 2    60   Input ~ 0
GND2
Text GLabel 6900 3600 2    60   Input ~ 0
VCC2+6V
Text GLabel 5700 5500 3    60   Input ~ 0
GNDHV
Text GLabel 5700 5100 1    60   Input ~ 0
EN/UV
Text GLabel 8800 3100 1    60   Input ~ 0
VCC2+24V
Text GLabel 8800 3600 3    60   Input ~ 0
GND2
Text GLabel 8300 3100 1    60   Input ~ 0
VCC1+6V
Text GLabel 8300 3600 3    60   Input ~ 0
GND1
$Comp
L CP C7
U 1 1 588951D3
P 9300 3350
F 0 "C7" H 9325 3450 50  0000 L CNN
F 1 "470u 25V" H 9100 3250 50  0000 L CNN
F 2 "Local:C_Radial_D8_L11.5_P3.5" H 9338 3200 30  0001 C CNN
F 3 "" H 9300 3350 60  0000 C CNN
	1    9300 3350
	1    0    0    -1  
$EndComp
Text GLabel 9300 3100 1    60   Input ~ 0
VCC2+6V
Text GLabel 9300 3600 3    60   Input ~ 0
GND2
$Comp
L GND #PWR01
U 1 1 57440002
P 1000 3250
F 0 "#PWR01" H 1000 3000 50  0001 C CNN
F 1 "GND" H 1000 3100 50  0000 C CNN
F 2 "" H 1000 3250 60  0000 C CNN
F 3 "" H 1000 3250 60  0000 C CNN
	1    1000 3250
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR02
U 1 1 5743FFF5
P 1300 3250
F 0 "#PWR02" H 1300 3000 50  0001 C CNN
F 1 "GND" H 1300 3100 50  0000 C CNN
F 2 "" H 1300 3250 60  0000 C CNN
F 3 "" H 1300 3250 60  0000 C CNN
	1    1300 3250
	1    0    0    -1  
$EndComp
$Comp
L PC817 U5
U 1 1 5889A496
P 7500 5300
F 0 "U5" H 7300 5500 50  0000 L CNN
F 1 "PC817" H 7500 5500 50  0000 L CNN
F 2 "Local:SODIP-4_W7.62mm" H 7300 5100 50  0001 L CIN
F 3 "" H 7500 5300 50  0000 L CNN
	1    7500 5300
	-1   0    0    -1  
$EndComp
Text GLabel 8000 4500 0    60   Input ~ 0
VCC2+24V
$Comp
L R R8
U 1 1 5889A49D
P 8100 5250
F 0 "R8" V 8180 5250 50  0000 C CNN
F 1 "1K" V 8100 5250 50  0000 C CNN
F 2 "Local:R_0603" V 8030 5250 30  0001 C CNN
F 3 "" H 8100 5250 30  0000 C CNN
	1    8100 5250
	-1   0    0    -1  
$EndComp
$Comp
L R R6
U 1 1 5889A4A3
P 8100 4750
F 0 "R6" V 8180 4750 50  0000 C CNN
F 1 "680" V 8100 4750 50  0000 C CNN
F 2 "Local:R_0603" V 8030 4750 30  0001 C CNN
F 3 "" H 8100 4750 30  0000 C CNN
	1    8100 4750
	-1   0    0    1   
$EndComp
Text GLabel 7100 5500 3    60   Input ~ 0
GNDHV
Text GLabel 7100 5100 1    60   Input ~ 0
EN/UV
Wire Wire Line
	6500 5200 6400 5200
Wire Wire Line
	6500 5000 6500 5200
Wire Wire Line
	6100 2900 6200 2900
Wire Wire Line
	5200 3100 5300 3100
Wire Wire Line
	5200 2400 5200 3100
Wire Wire Line
	4100 2400 5200 2400
Wire Wire Line
	5100 3500 5300 3500
Wire Wire Line
	5100 4000 5200 4000
Wire Wire Line
	5200 4000 5200 4800
Wire Wire Line
	5200 4300 5100 4300
Wire Wire Line
	5100 4200 5200 4200
Connection ~ 5200 4200
Wire Wire Line
	5100 4100 5200 4100
Connection ~ 5200 4100
Wire Wire Line
	5200 3900 5100 3900
Wire Wire Line
	5200 3500 5200 3900
Wire Wire Line
	4350 2400 4350 2500
Wire Wire Line
	4700 2300 4700 2500
Wire Wire Line
	4700 3100 4700 2800
Wire Wire Line
	4350 3400 4350 3500
Wire Wire Line
	4350 3500 4800 3500
Wire Wire Line
	4700 3500 4700 3400
Connection ~ 4700 2400
Connection ~ 4700 3500
Connection ~ 5200 3500
Wire Wire Line
	4300 3900 4200 3900
Wire Wire Line
	4200 3900 4200 4400
Wire Wire Line
	4200 4700 4200 4800
Wire Wire Line
	4200 4800 5200 4800
Wire Wire Line
	4700 4800 4700 4900
Wire Wire Line
	5700 5200 5800 5200
Wire Wire Line
	4100 3400 4100 4300
Wire Wire Line
	4100 4300 4300 4300
Wire Wire Line
	2400 2300 2500 2300
Wire Wire Line
	1900 2300 1500 2300
Connection ~ 5200 4300
Connection ~ 4700 4800
Wire Wire Line
	5700 5100 5700 5200
Wire Wire Line
	6100 3400 6200 3400
Wire Wire Line
	4350 2900 4350 3100
Wire Wire Line
	1300 3250 1300 3150
Wire Wire Line
	1000 3250 1000 3150
Wire Wire Line
	4100 2500 4100 2400
Connection ~ 4350 2400
Wire Wire Line
	4100 3100 4100 2800
Wire Notes Line
	3950 2650 3900 2650
Wire Notes Line
	3900 2650 3900 2850
Wire Notes Line
	3950 3250 3900 3250
Wire Notes Line
	3900 3250 3900 3050
Wire Wire Line
	2100 2900 2000 2900
Wire Wire Line
	2000 2900 2000 2800
Wire Wire Line
	3300 3050 3300 3150
Wire Wire Line
	3300 2600 3300 2750
Wire Wire Line
	2500 3300 2500 3400
Wire Wire Line
	2500 3400 1700 3400
Wire Wire Line
	1700 3400 1700 3200
Wire Wire Line
	1500 2400 1700 2400
Wire Wire Line
	1700 2400 1700 2700
Wire Wire Line
	2500 2300 2500 2500
Wire Wire Line
	2900 2900 3000 2900
Wire Wire Line
	3000 2900 3000 2800
Wire Wire Line
	6100 3200 6200 3200
Wire Wire Line
	6500 3100 6100 3100
Wire Wire Line
	6600 2900 6500 2900
Wire Wire Line
	6600 3400 6500 3400
Wire Wire Line
	7800 3100 7800 3200
Wire Wire Line
	7800 3500 7800 3600
Wire Wire Line
	6900 3100 6800 3100
Wire Wire Line
	6500 3600 6100 3600
Wire Wire Line
	6200 3700 6100 3700
Wire Wire Line
	6900 3600 6800 3600
Wire Wire Line
	5800 5400 5700 5400
Wire Wire Line
	5700 5400 5700 5500
Wire Wire Line
	8800 3100 8800 3200
Wire Wire Line
	8800 3500 8800 3600
Wire Wire Line
	8300 3100 8300 3200
Wire Wire Line
	8300 3500 8300 3600
Wire Wire Line
	9300 3100 9300 3200
Wire Wire Line
	9300 3500 9300 3600
Wire Wire Line
	7900 5200 7800 5200
Wire Wire Line
	7900 5000 7900 5200
Wire Wire Line
	7100 5200 7200 5200
Wire Wire Line
	7100 5100 7100 5200
Wire Wire Line
	7200 5400 7100 5400
Wire Wire Line
	7100 5400 7100 5500
Wire Wire Line
	6400 5400 6500 5400
Wire Wire Line
	6500 5400 6500 5500
Wire Wire Line
	6700 5400 6700 5600
Wire Wire Line
	6500 5500 6700 5500
Wire Wire Line
	6500 5000 6700 5000
Wire Wire Line
	6700 4900 6700 5100
$Comp
L ZENER D8
U 1 1 588994D1
P 6700 5800
F 0 "D8" H 6700 5900 50  0000 C CNN
F 1 "10V" H 6700 5700 50  0000 C CNN
F 2 "Local:MiniMELF_Standard" H 6700 5800 50  0001 C CNN
F 3 "" H 6700 5800 50  0000 C CNN
	1    6700 5800
	0    -1   1    0   
$EndComp
Connection ~ 6700 5500
Connection ~ 6700 5000
Wire Wire Line
	6700 4600 6700 4500
Wire Wire Line
	6700 4500 6600 4500
Wire Wire Line
	6700 6000 6700 6100
Text GLabel 6600 6100 0    60   Input ~ 0
GND1
Wire Wire Line
	6700 6100 6600 6100
$Comp
L ZENER D9
U 1 1 5889AEC4
P 8100 5800
F 0 "D9" H 8100 5900 50  0000 C CNN
F 1 "22V" H 8100 5700 50  0000 C CNN
F 2 "Local:MiniMELF_Standard" H 8100 5800 50  0001 C CNN
F 3 "" H 8100 5800 50  0000 C CNN
	1    8100 5800
	0    -1   1    0   
$EndComp
Text GLabel 8000 6100 0    60   Input ~ 0
GND2
Wire Wire Line
	7800 5400 7900 5400
Wire Wire Line
	7900 5400 7900 5500
Wire Wire Line
	7900 5500 8100 5500
Wire Wire Line
	8100 5400 8100 5600
Connection ~ 8100 5500
Wire Wire Line
	8100 6000 8100 6100
Wire Wire Line
	8100 6100 8000 6100
Wire Wire Line
	8100 4900 8100 5100
Wire Wire Line
	8100 5000 7900 5000
Connection ~ 8100 5000
Wire Wire Line
	8000 4500 8100 4500
Wire Wire Line
	8100 4500 8100 4600
Text GLabel 10500 2600 0    60   Input ~ 0
VCC1+12V
Text GLabel 10500 2700 0    60   Input ~ 0
GND1
Text GLabel 10500 3600 0    60   Input ~ 0
VCC2+24V
Text GLabel 10500 3700 0    60   Input ~ 0
GND2
Wire Wire Line
	10600 3600 10500 3600
Wire Wire Line
	10600 3700 10500 3700
Wire Wire Line
	10600 2600 10500 2600
Wire Wire Line
	10600 2700 10500 2700
Text GLabel 10500 1800 0    60   Input ~ 0
VCC1+6V
$Comp
L CONN_01X01 P1
U 1 1 5889CD5B
P 10800 1800
F 0 "P1" H 10800 1900 50  0000 C CNN
F 1 "6V" V 10900 1800 50  0000 C CNN
F 2 "Local:Pin_Header_Straight_1x01_Pitch2.54mm" H 10800 1800 50  0001 C CNN
F 3 "" H 10800 1800 50  0000 C CNN
	1    10800 1800
	1    0    0    1   
$EndComp
Wire Wire Line
	10600 1800 10500 1800
$Comp
L CONN_01X01 P2
U 1 1 5889D0F2
P 10800 2050
F 0 "P2" H 10800 2150 50  0000 C CNN
F 1 "6V" V 10900 2050 50  0000 C CNN
F 2 "Local:Pin_Header_Straight_1x01_Pitch2.54mm" H 10800 2050 50  0001 C CNN
F 3 "" H 10800 2050 50  0000 C CNN
	1    10800 2050
	1    0    0    1   
$EndComp
Text GLabel 10500 2050 0    60   Input ~ 0
VCC2+6V
Wire Wire Line
	10500 2050 10600 2050
$Comp
L LD1117S33TR U2
U 1 1 5889D675
P 9000 5250
F 0 "U2" H 9000 5500 50  0000 C CNN
F 1 "LD1117S33TR" H 9000 5450 50  0000 C CNN
F 2 "Local:SOT-223" H 9000 5350 50  0001 C CNN
F 3 "" H 9000 5250 50  0000 C CNN
	1    9000 5250
	1    0    0    -1  
$EndComp
Wire Wire Line
	9400 5200 9500 5200
Wire Wire Line
	9500 5100 9500 5300
Wire Wire Line
	9500 5600 9500 5700
Text GLabel 9500 5700 3    60   Input ~ 0
GND1
Wire Wire Line
	9000 5500 9000 5700
Text GLabel 9000 5700 3    60   Input ~ 0
GND1
Wire Wire Line
	8500 5200 8600 5200
Text GLabel 8500 5100 1    60   Input ~ 0
VCC1+6V
Wire Wire Line
	8500 5100 8500 5200
Connection ~ 9500 5200
Text GLabel 9500 5100 1    60   Input ~ 0
VCC1+3V3
$Comp
L CP C10
U 1 1 5889E6D5
P 10900 5450
F 0 "C10" H 10925 5550 50  0000 L CNN
F 1 "47u 25V" H 10750 5350 50  0000 L CNN
F 2 "Local:C_Radial_D5_L11_P2.5" H 10938 5300 30  0001 C CNN
F 3 "" H 10900 5450 60  0000 C CNN
	1    10900 5450
	1    0    0    -1  
$EndComp
$Comp
L LD1117S33TR U3
U 1 1 5889E6DB
P 10400 5250
F 0 "U3" H 10400 5500 50  0000 C CNN
F 1 "LD1117S33TR" H 10400 5450 50  0000 C CNN
F 2 "Local:SOT-223" H 10400 5350 50  0001 C CNN
F 3 "" H 10400 5250 50  0000 C CNN
	1    10400 5250
	1    0    0    -1  
$EndComp
Wire Wire Line
	10800 5200 10900 5200
Wire Wire Line
	10900 5100 10900 5300
Wire Wire Line
	10900 5600 10900 5700
Text GLabel 10900 5700 3    60   Input ~ 0
GND2
Wire Wire Line
	10400 5500 10400 5700
Text GLabel 10400 5700 3    60   Input ~ 0
GND2
Wire Wire Line
	9900 5200 10000 5200
Text GLabel 9900 5100 1    60   Input ~ 0
VCC2+6V
Wire Wire Line
	9900 5100 9900 5200
Connection ~ 10900 5200
Text GLabel 10900 5100 1    60   Input ~ 0
VCC2+3V3
$Comp
L CONN_01X02 P5
U 1 1 588A24C9
P 10800 3050
F 0 "P5" H 10800 3200 50  0000 C CNN
F 1 "3V3" V 10900 3050 50  0000 C CNN
F 2 "Local:Pin_Header_Straight_1x02_Pitch2.54mm" H 10800 3050 60  0001 C CNN
F 3 "" H 10800 3050 60  0000 C CNN
	1    10800 3050
	1    0    0    1   
$EndComp
Text GLabel 10500 3000 0    60   Input ~ 0
VCC1+3V3
Text GLabel 10500 3100 0    60   Input ~ 0
GND1
Wire Wire Line
	10600 3000 10500 3000
Wire Wire Line
	10600 3100 10500 3100
$Comp
L CONN_01X02 P7
U 1 1 588A270D
P 10800 4050
F 0 "P7" H 10800 4200 50  0000 C CNN
F 1 "3V3" V 10900 4050 50  0000 C CNN
F 2 "Local:Pin_Header_Straight_1x02_Pitch2.54mm" H 10800 4050 60  0001 C CNN
F 3 "" H 10800 4050 60  0000 C CNN
	1    10800 4050
	1    0    0    1   
$EndComp
Text GLabel 10500 4000 0    60   Input ~ 0
VCC2+3V3
Text GLabel 10500 4100 0    60   Input ~ 0
GND2
Wire Wire Line
	10600 4000 10500 4000
Wire Wire Line
	10600 4100 10500 4100
$Comp
L C C11
U 1 1 588AA970
P 6200 6850
F 0 "C11" H 6225 6950 50  0000 L CNN
F 1 "2n2 250V" H 6050 6750 50  0000 L CNN
F 2 "Local:C_Rect_L7_W4.5_P5" H 6238 6700 30  0001 C CNN
F 3 "" H 6200 6850 60  0000 C CNN
	1    6200 6850
	1    0    0    -1  
$EndComp
Text GLabel 5700 6600 1    60   Input ~ 0
GNDHV
Wire Wire Line
	5700 6600 5700 6700
Text GLabel 5700 7100 3    60   Input ~ 0
GND1
Wire Wire Line
	5700 7000 5700 7100
Wire Wire Line
	6200 7000 6200 7100
Text GLabel 6200 6600 1    60   Input ~ 0
GNDHV
Wire Wire Line
	6200 6600 6200 6700
Text GLabel 6200 7100 3    60   Input ~ 0
GND2
Text Notes 3800 1600 0    47   ~ 0
Transformer (v2):\n\nPrimary coil:\n  110T 30AWG (mm25) 1.25mH\n\nSecondary coil 24V: 31.66uH\n  Section 1 (6V): 4T 27AWG (mm35) 2.17uH\n  Section 2 (6V): 4T 27AWG (mm35) 18.35uH\n\nSecondary coil 12V: 8.46uH\n  Section 1 (6V): 4T 27AWG (mm35) 2.2uH\n  Section 2 (18V): 12T 27AWG (mm35) 2.0uH
Text Notes 2550 250  0    60   ~ 0
Transformer (v1):\n\nPrimary coil:\n  86T 30AWG (mm25) 1.425mH\n\nSecondary coil 24V:\n  Section 1 (6V): 4T 27AWG (mm35)\n  Section 2 (6V): 4T 27AWG (mm35)\n\nSecondary coil 12V:\n  Section 1 (6V): 4T 27AWG (mm35)\n  Section 2 (18V): 12T 27AWG (mm35)
Text Notes 5750 2100 0    47   ~ 0
/* Transformer coils */\n\nEQ[L]: L = A[L] * N^2;\nEQ[N]: solve(EQ[L], N)[2];\nEQ[A[L]]: solve(EQ[L], A[L])[1];\n\nPR[1]: [N=110, L=1.25e-3]$\nPR[2]: [N=16, L=31.66e-6]$\nPR[3]: [N=8, L=8.46e-6]$\n\nEQ[A[L]], PR[1];\nEQ[A[L]], PR[2];\nEQ[A[L]], PR[3];\n\nPR[A[L]]: A[L]=103e-9;\nPR[L]: L=1.25e-3;\nEQ[N], PR[A[L]], PR[L];\nPR[N]: N=110;\nEQ[L], PR[A[L]], PR[N];
$EndSCHEMATC

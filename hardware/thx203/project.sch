EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:addon
LIBS:project-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L CP C6
U 1 1 57DB9558
P 4500 4150
F 0 "C6" H 4525 4250 50  0000 L CNN
F 1 "47u 400V" H 4350 4050 50  0000 L CNN
F 2 "Local:C_Radial_D16_L25_P7.5" H 4538 4000 50  0001 C CNN
F 3 "" H 4500 4150 50  0000 C CNN
	1    4500 4150
	1    0    0    -1  
$EndComp
$Comp
L DB D4
U 1 1 57DB95EC
P 4000 3900
F 0 "D4" H 3750 4200 50  0000 C CNN
F 1 "DB107S" H 4000 3900 50  0000 C CNN
F 2 "Local:DB4S" H 4000 3900 50  0001 C CNN
F 3 "" H 4000 3900 50  0000 C CNN
	1    4000 3900
	1    0    0    1   
$EndComp
$Comp
L Earth #PWR01
U 1 1 57DB966D
P 3500 4000
F 0 "#PWR01" H 3500 3750 50  0001 C CNN
F 1 "Earth" H 3500 3850 50  0001 C CNN
F 2 "" H 3500 4000 50  0000 C CNN
F 3 "" H 3500 4000 50  0000 C CNN
	1    3500 4000
	1    0    0    -1  
$EndComp
$Comp
L Earth #PWR02
U 1 1 57DB9689
P 4500 4400
F 0 "#PWR02" H 4500 4150 50  0001 C CNN
F 1 "Earth" H 4500 4250 50  0001 C CNN
F 2 "" H 4500 4400 50  0000 C CNN
F 3 "" H 4500 4400 50  0000 C CNN
	1    4500 4400
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X02 P2
U 1 1 57DB976A
P 3000 4150
F 0 "P2" H 3000 4300 50  0000 C CNN
F 1 "250VAC" V 3100 4150 50  0000 C CNN
F 2 "Local:2PIN_HEAD" H 3000 4150 50  0001 C CNN
F 3 "" H 3000 4150 50  0000 C CNN
	1    3000 4150
	-1   0    0    -1  
$EndComp
$Comp
L F_Small F1
U 1 1 57DB97FC
P 3600 3400
F 0 "F1" H 3560 3460 50  0000 L CNN
F 1 "1A" H 3550 3350 50  0000 L CNN
F 2 "Local:FUSE_1A" H 3600 3400 50  0001 C CNN
F 3 "" H 3600 3400 50  0000 C CNN
	1    3600 3400
	1    0    0    -1  
$EndComp
$Comp
L R R5
U 1 1 57DB991A
P 4900 4050
F 0 "R5" V 4980 4050 50  0000 C CNN
F 1 "1M" V 4900 4050 50  0000 C CNN
F 2 "Local:R_1206" V 4830 4050 50  0001 C CNN
F 3 "" H 4900 4050 50  0000 C CNN
	1    4900 4050
	-1   0    0    -1  
$EndComp
$Comp
L R R9
U 1 1 57DB998E
P 4900 4450
F 0 "R9" V 4980 4450 50  0000 C CNN
F 1 "1M" V 4900 4450 50  0000 C CNN
F 2 "Local:R_1206" V 4830 4450 50  0001 C CNN
F 3 "" H 4900 4450 50  0000 C CNN
	1    4900 4450
	-1   0    0    -1  
$EndComp
$Comp
L THX203H U1
U 1 1 57DB9BED
P 3800 2550
F 0 "U1" H 3800 3000 60  0000 C CNN
F 1 "THX203H" V 3800 2550 60  0000 C CNN
F 2 "Local:DIP-8_W7.62mm" H 3800 2550 60  0001 C CNN
F 3 "" H 3800 2550 60  0000 C CNN
	1    3800 2550
	1    0    0    -1  
$EndComp
$Comp
L C C8
U 1 1 57DB9EE3
P 4900 4950
F 0 "C8" H 4925 5050 50  0000 L CNN
F 1 "10n" H 4925 4850 50  0000 L CNN
F 2 "Local:C_0603" H 4938 4800 50  0001 C CNN
F 3 "" H 4900 4950 50  0000 C CNN
	1    4900 4950
	1    0    0    -1  
$EndComp
$Comp
L Earth #PWR03
U 1 1 57DB9FA2
P 4900 5200
F 0 "#PWR03" H 4900 4950 50  0001 C CNN
F 1 "Earth" H 4900 5050 50  0001 C CNN
F 2 "" H 4900 5200 50  0000 C CNN
F 3 "" H 4900 5200 50  0000 C CNN
	1    4900 5200
	1    0    0    -1  
$EndComp
$Comp
L C C1
U 1 1 57DBA2B3
P 3050 2250
F 0 "C1" V 3100 2350 50  0000 L CNN
F 1 "680p" V 3000 2000 50  0000 L CNN
F 2 "Local:C_0603" H 3088 2100 50  0001 C CNN
F 3 "" H 3050 2250 50  0000 C CNN
	1    3050 2250
	0    -1   -1   0   
$EndComp
$Comp
L Earth #PWR04
U 1 1 57DBA316
P 2800 2350
F 0 "#PWR04" H 2800 2100 50  0001 C CNN
F 1 "Earth" H 2800 2200 50  0001 C CNN
F 2 "" H 2800 2350 50  0000 C CNN
F 3 "" H 2800 2350 50  0000 C CNN
	1    2800 2350
	1    0    0    -1  
$EndComp
$Comp
L Earth #PWR05
U 1 1 57DBAB30
P 3200 2950
F 0 "#PWR05" H 3200 2700 50  0001 C CNN
F 1 "Earth" H 3200 2800 50  0001 C CNN
F 2 "" H 3200 2950 50  0000 C CNN
F 3 "" H 3200 2950 50  0000 C CNN
	1    3200 2950
	1    0    0    -1  
$EndComp
Text Label 5250 4700 2    39   ~ 0
INIT_PWR
Text Label 4700 2850 2    39   ~ 0
INIT_PWR
Text Label 4700 2250 2    39   ~ 0
WORK_PWR
Text Label 3000 2450 0    39   ~ 0
FEEDBACK
Text Label 4700 2450 2    39   ~ 0
SWITCH
$Comp
L TRANS_3V T1
U 1 1 57DBB0CD
P 6200 2600
F 0 "T1" H 6200 3100 50  0000 C CNN
F 1 "TRANS_3X" H 6200 2100 50  0000 C CNN
F 2 "Local:TRANS_EE19_5" H 6200 2600 60  0001 C CNN
F 3 "" H 6200 2600 60  0000 C CNN
	1    6200 2600
	1    0    0    -1  
$EndComp
$Comp
L D D2
U 1 1 57DBB222
P 6850 2200
F 0 "D2" H 6850 2300 50  0000 C CNN
F 1 "1N4007" H 6850 2100 50  0000 C CNN
F 2 "Local:Diode_DO-41_SOD81_Vertical_KathodeUp" H 6850 2200 50  0001 C CNN
F 3 "" H 6850 2200 50  0000 C CNN
	1    6850 2200
	-1   0    0    -1  
$EndComp
$Comp
L Earth #PWR06
U 1 1 57DBB2BC
P 6700 2500
F 0 "#PWR06" H 6700 2250 50  0001 C CNN
F 1 "Earth" H 6700 2350 50  0001 C CNN
F 2 "" H 6700 2500 50  0000 C CNN
F 3 "" H 6700 2500 50  0000 C CNN
	1    6700 2500
	0    -1   -1   0   
$EndComp
Text Label 7400 2200 2    39   ~ 0
WORK_PWR
Text Label 5500 2800 0    39   ~ 0
SWITCH
Text Label 4500 3400 3    39   ~ 0
HV_PWR
Text Label 4900 3400 3    39   ~ 0
HV_PWR
Text Label 5500 2400 0    39   ~ 0
HV_PWR
$Comp
L C C2
U 1 1 57DBB933
P 5150 2500
F 0 "C2" V 5300 2450 50  0000 L CNN
F 1 "2n2 1kV" V 5100 2100 50  0000 L CNN
F 2 "Local:C_1206" H 5188 2350 50  0001 C CNN
F 3 "" H 5150 2500 50  0000 C CNN
	1    5150 2500
	0    -1   -1   0   
$EndComp
$Comp
L D D3
U 1 1 57DBB986
P 5150 2800
F 0 "D3" H 5150 2900 50  0000 C CNN
F 1 "FR107" H 5150 2700 50  0000 C CNN
F 2 "Local:Diode_DO-41_SOD81_Horizontal_RM10" H 5150 2800 50  0001 C CNN
F 3 "" H 5150 2800 50  0000 C CNN
	1    5150 2800
	1    0    0    -1  
$EndComp
$Comp
L R R2
U 1 1 57DBB9F7
P 5150 2200
F 0 "R2" V 5230 2200 50  0000 C CNN
F 1 "150K" V 5150 2200 50  0000 C CNN
F 2 "Local:R_1206" V 5080 2200 50  0001 C CNN
F 3 "" H 5150 2200 50  0000 C CNN
	1    5150 2200
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR07
U 1 1 57DBC4C2
P 6700 3000
F 0 "#PWR07" H 6700 2750 50  0001 C CNN
F 1 "GND" V 6700 2800 50  0000 C CNN
F 2 "" H 6700 3000 50  0000 C CNN
F 3 "" H 6700 3000 50  0000 C CNN
	1    6700 3000
	0    -1   -1   0   
$EndComp
$Comp
L D_Schottky D1
U 1 1 57DBC627
P 6850 2700
F 0 "D1" H 6850 2800 50  0000 C CNN
F 1 "SF28" H 6850 2600 50  0000 C CNN
F 2 "Local:Diode_DO-41_SOD81_Vertical_KathodeUp" H 6850 2700 50  0001 C CNN
F 3 "" H 6850 2700 50  0000 C CNN
	1    6850 2700
	-1   0    0    -1  
$EndComp
Text Label 7400 2700 2    39   ~ 0
OUT_PWR
$Comp
L CP C3
U 1 1 57DBC935
P 7800 2550
F 0 "C3" H 7825 2650 50  0000 L CNN
F 1 "1m 35V" H 7650 2450 50  0000 L CNN
F 2 "Local:C_Radial_D10_L20_P5" H 7838 2400 50  0001 C CNN
F 3 "" H 7800 2550 50  0000 C CNN
	1    7800 2550
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR08
U 1 1 57DBC98A
P 7800 2800
F 0 "#PWR08" H 7800 2550 50  0001 C CNN
F 1 "GND" H 7800 2650 50  0000 C CNN
F 2 "" H 7800 2800 50  0000 C CNN
F 3 "" H 7800 2800 50  0000 C CNN
	1    7800 2800
	1    0    0    -1  
$EndComp
Text Label 7800 2100 3    39   ~ 0
OUT_PWR
$Comp
L CONN_01X02 P1
U 1 1 57DBCDFC
P 8500 2550
F 0 "P1" H 8500 2700 50  0000 C CNN
F 1 "12VDC" V 8600 2550 50  0000 C CNN
F 2 "Local:Pin_Header_Straight_1x02" H 8500 2550 50  0001 C CNN
F 3 "" H 8500 2550 50  0000 C CNN
	1    8500 2550
	1    0    0    1   
$EndComp
$Comp
L GND #PWR09
U 1 1 57DBCF8A
P 8200 2800
F 0 "#PWR09" H 8200 2550 50  0001 C CNN
F 1 "GND" H 8200 2650 50  0000 C CNN
F 2 "" H 8200 2800 50  0000 C CNN
F 3 "" H 8200 2800 50  0000 C CNN
	1    8200 2800
	1    0    0    -1  
$EndComp
Text Label 8200 2100 3    39   ~ 0
OUT_PWR
Text Label 7800 3300 3    39   ~ 0
OUT_PWR
$Comp
L R R7
U 1 1 57DBD3F5
P 7800 4150
F 0 "R7" V 7880 4150 50  0000 C CNN
F 1 "33K" V 7800 4150 50  0000 C CNN
F 2 "Local:R_0603" V 7730 4150 50  0001 C CNN
F 3 "" H 7800 4150 50  0000 C CNN
	1    7800 4150
	-1   0    0    -1  
$EndComp
$Comp
L R R10
U 1 1 57DBD45C
P 7800 4950
F 0 "R10" V 7880 4950 50  0000 C CNN
F 1 "10K" V 7800 4950 50  0000 C CNN
F 2 "Local:R_0603" V 7730 4950 50  0001 C CNN
F 3 "" H 7800 4950 50  0000 C CNN
	1    7800 4950
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR010
U 1 1 57DBD54F
P 7800 5200
F 0 "#PWR010" H 7800 4950 50  0001 C CNN
F 1 "GND" H 7800 5050 50  0000 C CNN
F 2 "" H 7800 5200 50  0000 C CNN
F 3 "" H 7800 5200 50  0000 C CNN
	1    7800 5200
	1    0    0    -1  
$EndComp
$Comp
L TL431S U3
U 1 1 57DBD770
P 6900 4700
F 0 "U3" H 6800 4750 50  0000 C CNN
F 1 "TL431" V 7000 4700 50  0000 C CNN
F 2 "Local:SOT-23-R" V 6900 4700 60  0001 C CNN
F 3 "" V 6900 4700 60  0000 C CNN
	1    6900 4700
	-1   0    0    -1  
$EndComp
$Comp
L C C7
U 1 1 57DBD7F5
P 7550 4400
F 0 "C7" V 7700 4350 50  0000 L CNN
F 1 "100n" V 7400 4300 50  0000 L CNN
F 2 "Local:C_0603" V 7588 4250 50  0001 C CNN
F 3 "" H 7550 4400 50  0000 C CNN
	1    7550 4400
	0    -1   -1   0   
$EndComp
$Comp
L R R8
U 1 1 57DBD8FC
P 7150 4400
F 0 "R8" V 7230 4400 50  0000 C CNN
F 1 "1K" V 7150 4400 50  0000 C CNN
F 2 "Local:R_0603" V 7080 4400 50  0001 C CNN
F 3 "" H 7150 4400 50  0000 C CNN
	1    7150 4400
	0    -1   -1   0   
$EndComp
$Comp
L R R6
U 1 1 57DBDB6B
P 6900 4150
F 0 "R6" V 6980 4150 50  0000 C CNN
F 1 "5K6" V 6900 4150 50  0000 C CNN
F 2 "Local:R_0603" V 6830 4150 50  0001 C CNN
F 3 "" H 6900 4150 50  0000 C CNN
	1    6900 4150
	-1   0    0    -1  
$EndComp
Text Label 6900 3400 3    39   ~ 0
OUT_PWR
$Comp
L PC817 U2
U 1 1 57DBEDF9
P 6300 3800
F 0 "U2" H 6100 4000 50  0000 L CNN
F 1 "LTV-356T" H 6300 4000 50  0000 L CNN
F 2 "Local:OPTO4S" H 6100 3600 50  0001 L CIN
F 3 "" H 6300 3800 50  0000 L CNN
	1    6300 3800
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR011
U 1 1 57DBEE88
P 6900 5200
F 0 "#PWR011" H 6900 4950 50  0001 C CNN
F 1 "GND" H 6900 5050 50  0000 C CNN
F 2 "" H 6900 5200 50  0000 C CNN
F 3 "" H 6900 5200 50  0000 C CNN
	1    6900 5200
	1    0    0    -1  
$EndComp
$Comp
L R R4
U 1 1 57DBF107
P 6700 4150
F 0 "R4" V 6780 4150 50  0000 C CNN
F 1 "2K2" V 6700 4150 50  0000 C CNN
F 2 "Local:R_0603" V 6630 4150 50  0001 C CNN
F 3 "" H 6700 4150 50  0000 C CNN
	1    6700 4150
	-1   0    0    -1  
$EndComp
Text Label 6700 3400 3    39   ~ 0
OUT_PWR
$Comp
L R R3
U 1 1 57DBF71E
P 7800 3750
F 0 "R3" V 7880 3750 50  0000 C CNN
F 1 "5K6" V 7800 3750 50  0000 C CNN
F 2 "Local:R_0603" V 7730 3750 50  0001 C CNN
F 3 "" H 7800 3750 50  0000 C CNN
	1    7800 3750
	-1   0    0    -1  
$EndComp
Text Label 5600 3700 0    39   ~ 0
FEEDBACK
$Comp
L Earth #PWR012
U 1 1 57DBFC75
P 5900 4000
F 0 "#PWR012" H 5900 3750 50  0001 C CNN
F 1 "Earth" H 5900 3850 50  0001 C CNN
F 2 "" H 5900 4000 50  0000 C CNN
F 3 "" H 5900 4000 50  0000 C CNN
	1    5900 4000
	1    0    0    -1  
$EndComp
$Comp
L C C5
U 1 1 57DC0078
P 5400 3650
F 0 "C5" H 5425 3750 50  0000 L CNN
F 1 "22n" H 5425 3550 50  0000 L CNN
F 2 "Local:C_0603" H 5438 3500 50  0001 C CNN
F 3 "" H 5400 3650 50  0000 C CNN
	1    5400 3650
	1    0    0    -1  
$EndComp
Text Label 5400 3100 3    39   ~ 0
FEEDBACK
Text Label 2400 2700 3    39   ~ 0
ISENSE
$Comp
L Earth #PWR013
U 1 1 57DC07BA
P 2400 3800
F 0 "#PWR013" H 2400 3550 50  0001 C CNN
F 1 "Earth" H 2400 3650 50  0001 C CNN
F 2 "" H 2400 3800 50  0000 C CNN
F 3 "" H 2400 3800 50  0000 C CNN
	1    2400 3800
	1    0    0    -1  
$EndComp
$Comp
L Earth #PWR014
U 1 1 57DC0819
P 5400 3900
F 0 "#PWR014" H 5400 3650 50  0001 C CNN
F 1 "Earth" H 5400 3750 50  0001 C CNN
F 2 "" H 5400 3900 50  0000 C CNN
F 3 "" H 5400 3900 50  0000 C CNN
	1    5400 3900
	1    0    0    -1  
$EndComp
$Comp
L C C9
U 1 1 57DC12C6
P 6250 5100
F 0 "C9" V 6400 5050 50  0000 L CNN
F 1 "2n2 400V" V 6100 4900 50  0000 L CNN
F 2 "Local:C_1206" H 6288 4950 50  0001 C CNN
F 3 "" H 6250 5100 50  0000 C CNN
	1    6250 5100
	0    -1   -1   0   
$EndComp
$Comp
L Earth #PWR015
U 1 1 57DC1364
P 5900 5200
F 0 "#PWR015" H 5900 4950 50  0001 C CNN
F 1 "Earth" H 5900 5050 50  0001 C CNN
F 2 "" H 5900 5200 50  0000 C CNN
F 3 "" H 5900 5200 50  0000 C CNN
	1    5900 5200
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR016
U 1 1 57DC13E3
P 6600 5200
F 0 "#PWR016" H 6600 4950 50  0001 C CNN
F 1 "GND" H 6600 5050 50  0000 C CNN
F 2 "" H 6600 5200 50  0000 C CNN
F 3 "" H 6600 5200 50  0000 C CNN
	1    6600 5200
	1    0    0    -1  
$EndComp
$Comp
L CP C4
U 1 1 57DC1D09
P 7300 3450
F 0 "C4" H 7325 3550 50  0000 L CNN
F 1 "47u 16V" H 7150 3350 50  0000 L CNN
F 2 "Local:C_Radial_D5_L11_P2.5" H 7338 3300 50  0001 C CNN
F 3 "" H 7300 3450 50  0000 C CNN
	1    7300 3450
	1    0    0    -1  
$EndComp
$Comp
L Earth #PWR017
U 1 1 57DC229B
P 7300 3700
F 0 "#PWR017" H 7300 3450 50  0001 C CNN
F 1 "Earth" H 7300 3550 50  0001 C CNN
F 2 "" H 7300 3700 50  0000 C CNN
F 3 "" H 7300 3700 50  0000 C CNN
	1    7300 3700
	1    0    0    -1  
$EndComp
Text Label 7300 2900 3    39   ~ 0
WORK_PWR
$Comp
L R R11
U 1 1 57E53BFB
P 8200 3750
F 0 "R11" V 8280 3750 50  0000 C CNN
F 1 "4K7" V 8200 3750 50  0000 C CNN
F 2 "Local:R_0603" V 8130 3750 50  0001 C CNN
F 3 "" H 8200 3750 50  0000 C CNN
	1    8200 3750
	-1   0    0    -1  
$EndComp
Text Label 8200 3300 3    39   ~ 0
OUT_PWR
$Comp
L LED D5
U 1 1 57E53F3D
P 8200 4200
F 0 "D5" H 8200 4300 50  0000 C CNN
F 1 "LED" H 8200 4100 50  0000 C CNN
F 2 "Local:LED_0603" H 8200 4200 50  0001 C CNN
F 3 "" H 8200 4200 50  0000 C CNN
	1    8200 4200
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR018
U 1 1 57E54010
P 8200 4500
F 0 "#PWR018" H 8200 4250 50  0001 C CNN
F 1 "GND" H 8200 4350 50  0000 C CNN
F 2 "" H 8200 4500 50  0000 C CNN
F 3 "" H 8200 4500 50  0000 C CNN
	1    8200 4500
	1    0    0    -1  
$EndComp
Text Notes 9000 3550 0    60   ~ 0
/* pulse transformer */\n\nEQ[L]: L = A[L] * n^2;\nEQ[A[L]]: solve(EQ[L], A[L])[1];\n\nPR[n1]: n=180;\nPR[L1]: L=1.4e-3;\nPR[n2]: n=15;\nPR[L2]: L=27.7e-6;\nPR[n3]: n=11.5;\nPR[L3]: L=17.5e-6;\n\nEQ[A[L]], PR[n1], PR[L1];\nEQ[A[L]], PR[n2], PR[L2];\nEQ[A[L]], PR[n3], PR[L3];\n\nPR[A[L]]: A[L]=84e-9;\n\nEQ[L], PR[A[L]], PR[n1];\nEQ[L], PR[A[L]], PR[n2];\nEQ[L], PR[A[L]], PR[n3];
Text Notes 5900 2600 0    60   ~ 0
W1
Text Notes 6450 2850 0    60   ~ 0
W3
Text Notes 6450 2350 0    60   ~ 0
W2
Text Notes 5900 1950 0    60   ~ 0
12V Output\nEE19H PC40\n0.5mm air gap\nW1: 0.2mm 150T 1.4mH\nW2: 0.16mm 13T\nW3: 0.5mm 24T
Text Label 3000 2650 0    39   ~ 0
ISENSE
$Comp
L R R1
U 1 1 57E7D6C4
P 5150 1900
F 0 "R1" V 5230 1900 50  0000 C CNN
F 1 "150K" V 5150 1900 50  0000 C CNN
F 2 "Local:R_1206" V 5080 1900 50  0001 C CNN
F 3 "" H 5150 1900 50  0000 C CNN
	1    5150 1900
	0    -1   -1   0   
$EndComp
$Comp
L R R12
U 1 1 57DC0152
P 2400 3150
F 0 "R12" V 2480 3150 50  0000 C CNN
F 1 "1R" V 2400 3150 50  0000 C CNN
F 2 "Local:C_1206" V 2330 3150 50  0001 C CNN
F 3 "" H 2400 3150 50  0000 C CNN
	1    2400 3150
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3600 3900 3500 3900
Wire Wire Line
	3500 3900 3500 4000
Wire Wire Line
	4500 4300 4500 4400
Wire Wire Line
	4500 3400 4500 4000
Wire Wire Line
	4500 3900 4400 3900
Wire Wire Line
	4000 4300 4000 4400
Wire Wire Line
	4000 4400 3300 4400
Wire Wire Line
	3300 4400 3300 4200
Wire Wire Line
	3300 4200 3200 4200
Wire Wire Line
	4000 3500 4000 3400
Wire Wire Line
	3300 4100 3200 4100
Wire Wire Line
	4000 3400 3700 3400
Wire Wire Line
	3500 3400 3300 3400
Wire Wire Line
	4900 4200 4900 4300
Connection ~ 4500 3900
Wire Wire Line
	4900 3400 4900 3900
Wire Wire Line
	4900 4600 4900 4800
Wire Wire Line
	4900 5100 4900 5200
Wire Wire Line
	4900 4700 5250 4700
Connection ~ 4900 4700
Wire Wire Line
	4700 2850 4300 2850
Wire Wire Line
	2800 2350 2800 2250
Wire Wire Line
	2800 2250 2900 2250
Wire Wire Line
	3300 2250 3200 2250
Wire Wire Line
	4300 2250 4700 2250
Wire Wire Line
	3300 2850 3200 2850
Wire Wire Line
	3200 2850 3200 2950
Wire Wire Line
	3300 2450 3000 2450
Wire Wire Line
	4300 2450 4700 2450
Wire Wire Line
	6600 2200 6700 2200
Wire Wire Line
	7000 2200 7400 2200
Wire Wire Line
	5800 2800 5300 2800
Wire Wire Line
	5400 2400 5800 2400
Wire Wire Line
	4900 2800 5000 2800
Wire Wire Line
	4900 1900 4900 2800
Wire Wire Line
	4900 2500 5000 2500
Wire Wire Line
	5000 2200 4900 2200
Connection ~ 4900 2500
Wire Wire Line
	5300 2200 5400 2200
Wire Wire Line
	5400 1900 5400 2500
Wire Wire Line
	5400 2500 5300 2500
Connection ~ 5400 2400
Wire Wire Line
	6600 3000 6700 3000
Wire Wire Line
	6600 2700 6700 2700
Wire Wire Line
	7000 2700 7400 2700
Wire Wire Line
	7800 2700 7800 2800
Wire Wire Line
	7800 2400 7800 2100
Wire Wire Line
	8300 2600 8200 2600
Wire Wire Line
	8200 2600 8200 2800
Wire Wire Line
	8300 2500 8200 2500
Wire Wire Line
	8200 2500 8200 2100
Wire Wire Line
	7800 3300 7800 3600
Wire Wire Line
	7800 4300 7800 4800
Wire Wire Line
	7800 5100 7800 5200
Wire Wire Line
	7800 4400 7700 4400
Connection ~ 7800 4400
Wire Wire Line
	6700 4400 7000 4400
Wire Wire Line
	6900 4300 6900 4500
Wire Wire Line
	6900 4000 6900 3400
Wire Wire Line
	7100 4700 7800 4700
Connection ~ 7800 4700
Wire Wire Line
	7400 4400 7300 4400
Connection ~ 6900 4400
Wire Wire Line
	6900 4900 6900 5200
Wire Wire Line
	6700 3700 6700 3400
Wire Wire Line
	7800 3900 7800 4000
Wire Wire Line
	6000 3900 5900 3900
Wire Wire Line
	5900 3900 5900 4000
Wire Wire Line
	6000 3700 5600 3700
Wire Wire Line
	2400 3700 2400 3800
Wire Wire Line
	5400 3800 5400 3900
Wire Wire Line
	5400 3500 5400 3100
Wire Wire Line
	2400 3000 2400 2700
Wire Wire Line
	6400 5100 6600 5100
Wire Wire Line
	6600 5100 6600 5200
Wire Wire Line
	6100 5100 5900 5100
Wire Wire Line
	5900 5100 5900 5200
Wire Wire Line
	7300 3600 7300 3700
Wire Wire Line
	7300 3300 7300 2900
Wire Wire Line
	8200 3300 8200 3600
Wire Wire Line
	8200 3900 8200 4000
Wire Wire Line
	8200 4400 8200 4500
Wire Wire Line
	4300 2650 4400 2650
Wire Wire Line
	4400 2650 4400 2450
Connection ~ 4400 2450
Wire Wire Line
	3300 2650 3000 2650
Wire Wire Line
	5300 1900 5400 1900
Connection ~ 5400 2200
Wire Wire Line
	5000 1900 4900 1900
Connection ~ 4900 2200
$Comp
L R R13
U 1 1 57E7EFAB
P 2400 3550
F 0 "R13" V 2480 3550 50  0000 C CNN
F 1 "R27" V 2400 3550 50  0000 C CNN
F 2 "Local:C_1206" V 2330 3550 50  0001 C CNN
F 3 "" H 2400 3550 50  0000 C CNN
	1    2400 3550
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2400 3300 2400 3400
Text Notes 8700 6150 0    60   ~ 0
/* reference divider */\nEQ[DIV]: R[1]/(R[1]+R[2]) = U[REF] / U[OUT]$\nEQ[R[2]]: solve(EQ[DIV], R[2])[1]$\nEQ[V[OUT]]: solve(EQ[DIV], U[OUT])[1]$\nPR[TL431]: U[REF]=2.5$\n\nPR[OUT12]: U[OUT]=12$\nPR[R[1]]: R[1]=10e3$\nEQ[R[2]], PR[TL431], PR[OUT12], PR[R[1]];\nPR[R[2]]: R[2]=33e3+5.6e3$\nEQ[V[OUT]], PR[TL431], PR[R[1]], PR[R[2]];\n\nPR[OUT24]: U[OUT]=24$\nPR[R[1]]: R[1]=4.7e3$\nEQ[R[2]], PR[TL431], PR[OUT24], PR[R[1]];\nPR[R[2]]: R[2]=39e3+1.5e3$\nEQ[V[OUT]], PR[TL431], PR[R[1]], PR[R[2]];\n\nPR[OUT24]: U[OUT]=24$\nPR[R[1]]: R[1]=8.2e3$\nEQ[R[2]], PR[TL431], PR[OUT24], PR[R[1]];\nPR[R[2]]: R[2]=68e3+3.3e3$\nEQ[V[OUT]], PR[TL431], PR[R[1]], PR[R[2]];
Text Notes 7550 4850 0    60   ~ 0
R[1]
Text Notes 7550 4000 0    60   ~ 0
R[2]
Text Notes 650  2550 0    60   ~ 0
/* switching frequency */\nEQ[F[S]]: F[S] = (C[T] * 24e3)^-1;\nEQ[C[T]]: solve(EQ[F[S]], C[T])[1];\nPR[REF]: F[S] = 60e3;\nEQ[C[T]], PR[REF];\nEQ[F[S]], C[T] = 680e-12 /* 61kHz */;
Text Notes 5050 5000 0    60   ~ 0
OPT?
Text Notes 2750 1700 0    60   ~ 0
/* current sense and limiting */\nEQ[I[LIM]]: I[LIM] = V[CMP] / (R[IS(INT)]^-1 + R[IS(EXT)]^-1)^-1;\nEQ[R[IS(EXT)]]: solve(EQ[I[LIM]], R[IS(EXT)])[1];\nEQ[P[R[IS(EXT)]]]: P[R[IS(EXT)]] = (I[LIM]/2)^2 * R[IS(EXT)]; \nPR[THX203H]: [V[CMP]=0.6, R[IS(INT)]=5]$\nPR[I[LIM]]: I[LIM]=0.65$\nEQ[R[IS(EXT)]], PR[THX203H], PR[I[LIM]];\nPR[R[IS(EXT)]]: R[IS(EXT)]=1/(1/1.5+1/2.2) /* reference example */;\nPR[I[LIM(RES)]]: EQ[I[LIM]], PR[THX203H], PR[R[IS(EXT)]];\nPR[R[IS(EXT)]]: R[IS(EXT)]=1+0.1;\nPR[I[LIM(RES)]]: EQ[I[LIM]], PR[THX203H], PR[R[IS(EXT)]];\nEQ[P[R[IS(EXT)]]], PR[I[LIM(RES)]], PR[R[IS(EXT)]];
Text Notes 700  9050 0    60   ~ 0
/* ferrite formfactor power */\n/* P[T] - formfactor power of transformer */\n/* C[S] - current shape factor (1.11 for sinus, 1 for meander) */\n/* C[M] - magnetic wire filling factor (0.6 - 0.95) */\n/* C[C] - wire coil filling factor (0.35) */\n/* C[P] - primary coil section factor (0.5 for transformers) */\n/* S[M] - magnetic wire section (m^2) */\n/* S[C] - magnetic window section (m^2) */\n/* J - current density (A/m^2, 3.5e6 for static environment, 6e6 for active cooling) */\n/* B - working magnetic flux density (Tesla) */\n/* F - working frequency (Hz) */\n/* W[P] - primary coil turns */\n/* U[P] - primary coil voltage */\n/* W[S] - secondary coil turns */\n/* U[S] - secondary coil voltage */\n/* C[O] - coefficient of performance */\n/* S[N] - single wire section (skin-effect related) */\n\nEQ[P[T]]: P[T] = 4 * C[S] * C[M] * C[C] * C[P] * S[M] * S[C] * J * B * F$\nEQ[W[P]]: W[P] = U[P] / (4 * C[S] * F * B * S[M] * C[M])$\nEQ[W[S]]: W[S] = U[S] * 1.05 * W[P] / U[P], EQ[W[P]]$\nEQ[S[P]]: S[P] = P[P] / (U[P] * J), P[P] = P[S] / C[O]$\nEQ[S[S]]: S[S] = P[S] / (U[S] * J)$\nEQ[S[N]]: S[N] = 14e-3 / F$\nEQ[D]: solve(S = %pi * D^2 / 4, D)[2]$\nEQ[D[P]]: D[P] = 2 * sqrt(S[P] / %pi), EQ[S[P]]$\nEQ[D[S]]: D[S] = 2 * sqrt(S[S] / %pi), EQ[S[S]]$\nEQ[D[N]]: D[N] = 2 * sqrt(S[N] / %pi), EQ[S[N]]$\n\nPR[IN]: [C[S]=1, J=3.5e6, B=0.3, F=63e3, C[P]=0.5, U[P]=310, U[S]=24, P[S]=20, C[O]=0.85]$\nPR[EE16]: [C[M]=0.6, C[C]=0.35, S[M]=4.85e-3*4e-3, S[C]=8.8e-3*2.56e-3]$\nPR[EE19]: [C[M]=0.6, C[C]=0.35, S[M]=4.85e-3^2, S[C]=9.2e-3*(13.5e-3-6.55e-3)/2]$\nPR[EE25]: [C[M]=0.6, C[C]=0.35, S[M]=6.25e-3*6.4e-3, S[C]=10.7e-3*(17.5e-3-9.2e-3)/2]$\n\nEQ[D[P]], PR[IN], numer;\nEQ[D[S]], PR[IN], numer;\nEQ[D[N]], PR[IN], numer;\n\nEQ[P[T]], PR[IN], PR[EE16];\nEQ[W[P]], PR[IN], PR[EE16];\nEQ[W[S]], PR[IN], PR[EE16];\n\nEQ[P[T]], PR[IN], PR[EE19];\nEQ[W[P]], PR[IN], PR[EE19];\nEQ[W[S]], PR[IN], PR[EE19];\n\nEQ[P[T]], PR[IN], PR[EE25];\nEQ[W[P]], PR[IN], PR[EE25];\nEQ[W[S]], PR[IN], PR[EE25];
Wire Wire Line
	6700 2500 6600 2500
Wire Wire Line
	6600 3900 6700 3900
Wire Wire Line
	6700 3900 6700 4000
Wire Wire Line
	6700 3700 6600 3700
Wire Wire Line
	6700 4400 6700 4300
Text Notes 9000 1400 0    60   ~ 0
/* primary coil EE19 */\nEQ[L]: L = A[L] * N^2;\nEQ[A[L]]: solve(EQ[L], A[L])[1];\nEQ[N]: solve(EQ[L], N)[2];\n\nPR[T[1]]: [N=108, L=1.28e-3]$\nPR[L[1]]: L=1.4e-3;\nPR[A[L]]: EQ[A[L]], PR[T[1]];\nEQ[N], PR[A[L]], PR[L[1]];
Text Notes 5450 7250 0    60   ~ 0
/* U[F] - optotrans forward voltage */\n/* I[F] - optotrans forward current */\n/* I[Z] - tl431 output current */\n\nEQ[R[4]]: R[4] = (U[OUT] - U[F]) / I[F];\nEQ[R[6]]: R[6] = U[OUT] / (I[Z] - I[F]);\n\nPR[OPT]: [U[F]=1.2, I[F]=5e-3, I[Z]=7.35e-3]$\nPR[OUT5]: [U[OUT]=5]$\nPR[OUT12]: [U[OUT]=12]$\nPR[OUT24]: [U[OUT]=24]$\n\nEQ[R[4]], PR[OPT], PR[OUT5];\nEQ[R[6]], PR[OPT], PR[OUT5];\nEQ[R[4]], PR[OPT], PR[OUT12];\nEQ[R[6]], PR[OPT], PR[OUT12];\nEQ[R[4]], PR[OPT], PR[OUT24];\nEQ[R[6]], PR[OPT], PR[OUT24];
$Comp
L THERMISTOR TH1
U 1 1 57EBFED9
P 3300 3750
F 0 "TH1" V 3400 3800 50  0000 C CNN
F 1 "10R" V 3200 3750 50  0000 C BNN
F 2 "Local:NTC_11x5.5_f7.5" H 3300 3750 50  0001 C CNN
F 3 "" H 3300 3750 50  0000 C CNN
	1    3300 3750
	-1   0    0    1   
$EndComp
Wire Wire Line
	3300 3400 3300 3500
Wire Wire Line
	3300 4100 3300 4000
Text Notes -4250 4650 0    60   ~ 0
/* Flyback calculation */\n\n/* U, I - voltages and currents */\n/* IN, OUT - input and output */\n/* PRI, SEC - primary and secondary */\n/* T[ON] - on-state time */\n/* N - transformation ratio */\n/* K[EFF] - coefficient of effeciency */\n\nEQ[T[ON]]: T[ON] = D / F$\nEQ[P[OUT]]: P[OUT] = U[OUT] * I[OUT]$\nEQ[P[IN]]: P[IN] = P[OUT] / K[EFF], EQ[P[OUT]]$\nEQ[I[IN(avg)]]: I[IN(avg)] = P[IN] / U[IN(min)], EQ[P[IN]]$\nEQ[I[IN(peak)]]: I[IN(peak)] = 2 * I[IN(avg)] / D, EQ[I[IN(avg)]]$\nEQ[L[PRI]]: L[PRI] = U[IN(min)] * T[ON] / I[IN(peak)], EQ[I[IN(peak)]], EQ[T[ON]]$\nEQ[N[SEC]]: N[SEC] = U[OUT] / U[IN(min)]$\n\nPR[IN]: [U[IN(min)]=90, U[IN(max)]=340, U[OUT]=24, I[OUT]=0.5, F=63e3, D=0.5, K[EFF]=0.85]$\n\nEQ[P[OUT]], PR[IN];\nEQ[P[IN]], PR[IN];\nEQ[I[IN(avg)]], PR[IN];\nEQ[I[IN(peak)]], PR[IN];\nEQ[L[PRI]], PR[IN];\nEQ[N[SEC]], PR[IN], numer;
Text Notes 6800 4000 1    47   ~ 0
4K7 (24V)
Text Notes 7000 4000 1    47   ~ 0
10K (24V)
Text Notes 7950 5100 1    47   ~ 0
8K2 (24V)
Text Notes 7950 4350 1    47   ~ 0
68K (24V)
Text Notes 7950 3900 1    47   ~ 0
3K3 (24V)
Text Notes 7000 4550 0    47   ~ 0
2K2 (24V)
Text Notes 8350 3900 1    47   ~ 0
10K (24V)
Text Notes 7350 1850 0    60   ~ 0
24V Output V2\nEE19H PC40\n0.5mm air gap\nW1: 0.2mm 2X 120T 1.7mH\nW2: 0.16mm 1X 11T\nW3: 0.5mm 1X 39T
Text Notes 5550 9150 0    47   ~ 0
/* wire length */\n\n/* n[0] - turns per layer */\n/* h[0] - layer thickness */\n/* n - number of turns */\n/* l[0] - initial coil length */\n\nEQ[WL]: l = l[0]*n + h[0]*4*(ceiling(n/n[0])*(ceiling(n/n[0])-1)/2+0.5)*n[0] - h[0]*4*(ceiling(n/n[0])-1+0.5)*mod(n, n[0]);\nPR[IN0]: [n[0]=150, h[0]=0.3, l[0]=(6.7+7.2)*2, n=150]$\nPR[IN1]: [n[0]=42/2, h[0]=0.3, l[0]=(6.7+7.2)*2, n=150]$\nPR[IN2]: [n[0]=42, h[0]=0.3*2, l[0]=(6.7+7.2)*2, n=150]$\n\nEQ[WL], PR[IN0], numer;\nEQ[WL], PR[IN1], numer;\nEQ[WL], PR[IN2], numer;
$EndSCHEMATC

EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:addon
LIBS:project-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L TL431 U3
U 1 1 571D1166
P 8700 4800
F 0 "U3" H 8600 4850 50  0000 C CNN
F 1 "TL431" V 8800 4800 50  0000 C CNN
F 2 "Local:TO-92_Inline_Wide" V 8700 4800 60  0001 C CNN
F 3 "" V 8700 4800 60  0000 C CNN
	1    8700 4800
	-1   0    0    -1  
$EndComp
$Comp
L R R7
U 1 1 571D1282
P 8950 4300
F 0 "R7" V 9030 4300 50  0000 C CNN
F 1 "4K7" V 8950 4300 50  0000 C CNN
F 2 "Local:R_0603" V 8880 4300 30  0001 C CNN
F 3 "" H 8950 4300 30  0000 C CNN
	1    8950 4300
	0    -1   -1   0   
$EndComp
$Comp
L C C9
U 1 1 571D12D1
P 9200 4550
F 0 "C9" H 9225 4650 50  0000 L CNN
F 1 "100n" H 9225 4450 50  0000 L CNN
F 2 "Local:C_0603" H 9238 4400 30  0001 C CNN
F 3 "" H 9200 4550 60  0000 C CNN
	1    9200 4550
	1    0    0    -1  
$EndComp
$Comp
L R R9
U 1 1 571D1358
P 9500 5050
F 0 "R9" V 9580 5050 50  0000 C CNN
F 1 "10K" V 9500 5050 50  0000 C CNN
F 2 "Local:R_0603" V 9430 5050 30  0001 C CNN
F 3 "" H 9500 5050 30  0000 C CNN
	1    9500 5050
	-1   0    0    1   
$EndComp
$Comp
L R R8
U 1 1 571D13B8
P 9500 4550
F 0 "R8" V 9580 4550 50  0000 C CNN
F 1 "39K" V 9500 4550 50  0000 C CNN
F 2 "Local:R_0603" V 9430 4550 30  0001 C CNN
F 3 "" H 9500 4550 30  0000 C CNN
	1    9500 4550
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR01
U 1 1 571D156A
P 8700 5300
F 0 "#PWR01" H 8700 5050 50  0001 C CNN
F 1 "GND" H 8700 5150 50  0000 C CNN
F 2 "" H 8700 5300 60  0000 C CNN
F 3 "" H 8700 5300 60  0000 C CNN
	1    8700 5300
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR02
U 1 1 571D1588
P 9500 5700
F 0 "#PWR02" H 9500 5450 50  0001 C CNN
F 1 "GND" H 9500 5550 50  0000 C CNN
F 2 "" H 9500 5700 60  0000 C CNN
F 3 "" H 9500 5700 60  0000 C CNN
	1    9500 5700
	1    0    0    -1  
$EndComp
$Comp
L PC817 U2
U 1 1 571D1672
P 7800 4400
F 0 "U2" H 7600 4600 50  0000 L CNN
F 1 "PC817" H 7800 4600 50  0000 L CNN
F 2 "Local:DIP-4_W7.62mm" H 7600 4200 50  0001 L CIN
F 3 "" H 7800 4400 50  0000 L CNN
	1    7800 4400
	-1   0    0    -1  
$EndComp
$Comp
L D_Schottky D4
U 1 1 571D18D1
P 8450 2600
F 0 "D4" H 8450 2700 50  0000 C CNN
F 1 "US1D" H 8450 2500 50  0000 C CNN
F 2 "Local:SMB_Standard" H 8450 2600 60  0001 C CNN
F 3 "" H 8450 2600 60  0000 C CNN
	1    8450 2600
	-1   0    0    -1  
$EndComp
$Comp
L CP C4
U 1 1 571D1A26
P 9000 2850
F 0 "C4" H 9025 2950 50  0000 L CNN
F 1 "470u 25V" H 8800 2750 50  0000 L CNN
F 2 "Local:C_Radial_D10_L20_P5" H 9038 2700 30  0001 C CNN
F 3 "" H 9000 2850 60  0000 C CNN
	1    9000 2850
	1    0    0    -1  
$EndComp
$Comp
L TRANS_3X T1
U 1 1 571D1D9E
P 7700 3000
F 0 "T1" H 7600 3350 50  0000 C CNN
F 1 "PC40 EE19 GAP 100um" H 7900 2500 50  0000 C CNN
F 2 "Local:TRANS_EE19_5" H 7700 3000 60  0001 C CNN
F 3 "" H 7700 3000 60  0000 C CNN
	1    7700 3000
	1    0    0    -1  
$EndComp
$Comp
L C C1
U 1 1 571D205F
P 7450 2100
F 0 "C1" H 7475 2200 50  0000 L CNN
F 1 "2n2 250V" V 7550 1650 50  0000 L CNN
F 2 "Local:C_Rect_L7_W4.5_P5" H 7488 1950 30  0001 C CNN
F 3 "" H 7450 2100 60  0000 C CNN
	1    7450 2100
	0    -1   -1   0   
$EndComp
$Comp
L TNY278PN D9
U 1 1 571D2546
P 6700 3800
F 0 "D9" H 6700 4150 60  0000 C CNN
F 1 "TNY278PN" H 6700 3450 60  0000 C CNN
F 2 "Local:DIP-8_n3_W7.62mm" H 6700 3800 60  0001 C CNN
F 3 "" H 6700 3800 60  0000 C CNN
	1    6700 3800
	1    0    0    -1  
$EndComp
Text GLabel 6700 4600 3    60   Input ~ 0
HV_GND
$Comp
L ZENER D3
U 1 1 571D2984
P 6350 2400
F 0 "D3" H 6350 2500 50  0000 C CNN
F 1 "P6KE150A" H 6350 2300 50  0000 C CNN
F 2 "Local:Diode_DO-41_SOD81_Vertical_KathodeUp" H 6350 2400 60  0001 C CNN
F 3 "" H 6350 2400 60  0000 C CNN
	1    6350 2400
	0    -1   -1   0   
$EndComp
Text GLabel 7200 2000 1    60   Input ~ 0
HV_VCC
$Comp
L R R1
U 1 1 571D2BF6
P 6700 2350
F 0 "R1" V 6780 2350 50  0000 C CNN
F 1 "100" V 6700 2350 50  0000 C CNN
F 2 "Local:R_1206" V 6630 2350 30  0001 C CNN
F 3 "" H 6700 2350 30  0000 C CNN
	1    6700 2350
	-1   0    0    1   
$EndComp
$Comp
L C C5
U 1 1 571D2D0C
P 6700 2950
F 0 "C5" H 6725 3050 50  0000 L CNN
F 1 "10n 1kV" H 6550 2850 50  0000 L CNN
F 2 "Local:C_Rect_L7_W4.5_P5" H 6738 2800 30  0001 C CNN
F 3 "" H 6700 2950 60  0000 C CNN
	1    6700 2950
	1    0    0    -1  
$EndComp
$Comp
L R R2
U 1 1 571D2E61
P 6350 2950
F 0 "R2" V 6430 2950 50  0000 C CNN
F 1 "1K" V 6350 2950 50  0000 C CNN
F 2 "Local:R_1206" V 6280 2950 30  0001 C CNN
F 3 "" H 6350 2950 30  0000 C CNN
	1    6350 2950
	-1   0    0    1   
$EndComp
$Comp
L D D8
U 1 1 571D2F9E
P 6950 3200
F 0 "D8" H 6950 3300 50  0000 C CNN
F 1 "FR107" H 6950 3100 50  0000 C CNN
F 2 "Local:Diode_DO-41_SOD81_Vertical_KathodeUp" H 6950 3200 60  0001 C CNN
F 3 "" H 6950 3200 60  0000 C CNN
	1    6950 3200
	1    0    0    -1  
$EndComp
$Comp
L C C8
U 1 1 571D3A6B
P 6200 4250
F 0 "C8" H 6225 4350 50  0000 L CNN
F 1 "100n 50V" H 6000 4150 50  0000 L CNN
F 2 "Local:C_0805" H 6238 4100 30  0001 C CNN
F 3 "" H 6200 4250 60  0000 C CNN
	1    6200 4250
	1    0    0    -1  
$EndComp
$Comp
L D D1
U 1 1 571D4573
P 4550 2200
F 0 "D1" H 4550 2300 50  0000 C CNN
F 1 "1N4007" H 4550 2100 50  0000 C CNN
F 2 "Local:Diode_DO-41_SOD81_Vertical_KathodeUp" H 4550 2200 60  0001 C CNN
F 3 "" H 4550 2200 60  0000 C CNN
	1    4550 2200
	-1   0    0    -1  
$EndComp
$Comp
L D D2
U 1 1 571D46CE
P 4550 2500
F 0 "D2" H 4550 2600 50  0000 C CNN
F 1 "1N4007" H 4550 2400 50  0000 C CNN
F 2 "Local:Diode_DO-41_SOD81_Vertical_KathodeUp" H 4550 2500 60  0001 C CNN
F 3 "" H 4550 2500 60  0000 C CNN
	1    4550 2500
	-1   0    0    -1  
$EndComp
$Comp
L D D5
U 1 1 571D4849
P 4550 2800
F 0 "D5" H 4550 2900 50  0000 C CNN
F 1 "1N4007" H 4550 2700 50  0000 C CNN
F 2 "Local:Diode_DO-41_SOD81_Vertical_KathodeUp" H 4550 2800 60  0001 C CNN
F 3 "" H 4550 2800 60  0000 C CNN
	1    4550 2800
	1    0    0    -1  
$EndComp
$Comp
L D D6
U 1 1 571D484F
P 4550 3100
F 0 "D6" H 4550 3200 50  0000 C CNN
F 1 "1N4007" H 4550 3000 50  0000 C CNN
F 2 "Local:Diode_DO-41_SOD81_Vertical_KathodeUp" H 4550 3100 60  0001 C CNN
F 3 "" H 4550 3100 60  0000 C CNN
	1    4550 3100
	1    0    0    -1  
$EndComp
$Comp
L FUSE F1
U 1 1 571D4CE6
P 3850 2200
F 0 "F1" H 3950 2250 50  0000 C CNN
F 1 "3A" H 3750 2150 50  0000 C CNN
F 2 "Local:FUSE_3A" H 3850 2200 60  0001 C CNN
F 3 "" H 3850 2200 60  0000 C CNN
	1    3850 2200
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X02 P1
U 1 1 571D520B
P 3200 2250
F 0 "P1" H 3200 2400 50  0000 C CNN
F 1 "PWR" V 3300 2250 50  0000 C CNN
F 2 "Local:2PIN_HEAD" H 3200 2250 60  0001 C CNN
F 3 "" H 3200 2250 60  0000 C CNN
	1    3200 2250
	-1   0    0    -1  
$EndComp
$Comp
L CP C3
U 1 1 571D5FC0
P 5000 2650
F 0 "C3" H 5025 2750 50  0000 L CNN
F 1 "47u 400V" H 4850 2550 50  0000 L CNN
F 2 "Local:C_Radial_D10_L20_P5" H 5038 2500 30  0001 C CNN
F 3 "" H 5000 2650 60  0000 C CNN
	1    5000 2650
	1    0    0    -1  
$EndComp
Text GLabel 5000 2100 1    60   Input ~ 0
HV_VCC
Text GLabel 5000 3200 3    60   Input ~ 0
HV_GND
$Comp
L CONN_01X02 P2
U 1 1 571D735E
P 9500 2650
F 0 "P2" H 9500 2800 50  0000 C CNN
F 1 "+12V" V 9600 2650 50  0000 C CNN
F 2 "Local:Pin_Header_Straight_1x02" H 9500 2650 60  0001 C CNN
F 3 "" H 9500 2650 60  0000 C CNN
	1    9500 2650
	1    0    0    1   
$EndComp
$Comp
L GND #PWR03
U 1 1 571D7837
P 9700 3700
F 0 "#PWR03" H 9700 3450 50  0001 C CNN
F 1 "GND" H 9700 3550 50  0000 C CNN
F 2 "" H 9700 3700 60  0000 C CNN
F 3 "" H 9700 3700 60  0000 C CNN
	1    9700 3700
	1    0    0    -1  
$EndComp
Text GLabel 9000 2500 1    60   Input ~ 0
+12VR
Text GLabel 8100 3800 0    60   Input ~ 0
+12VR
$Comp
L GND #PWR04
U 1 1 571DA6C6
P 7700 2200
F 0 "#PWR04" H 7700 1950 50  0001 C CNN
F 1 "GND" H 7700 2050 50  0000 C CNN
F 2 "" H 7700 2200 60  0000 C CNN
F 3 "" H 7700 2200 60  0000 C CNN
	1    7700 2200
	1    0    0    -1  
$EndComp
$Comp
L R R5
U 1 1 571E341B
P 8200 4050
F 0 "R5" V 8280 4050 50  0000 C CNN
F 1 "1K" V 8200 4050 50  0000 C CNN
F 2 "Local:R_0603" V 8130 4050 30  0001 C CNN
F 3 "" H 8200 4050 30  0000 C CNN
	1    8200 4050
	-1   0    0    -1  
$EndComp
$Comp
L R R4
U 1 1 571E3FEC
P 7400 3850
F 0 "R4" V 7480 3850 50  0000 C CNN
F 1 "1K" V 7400 3850 50  0000 C CNN
F 2 "Local:R_0603" V 7330 3850 30  0001 C CNN
F 3 "" H 7400 3850 30  0000 C CNN
	1    7400 3850
	-1   0    0    1   
$EndComp
$Comp
L D_Schottky D7
U 1 1 571E5BCC
P 8450 3100
F 0 "D7" H 8450 3200 50  0000 C CNN
F 1 "US1D" H 8450 3000 50  0000 C CNN
F 2 "Local:SMB_Standard" H 8450 3100 60  0001 C CNN
F 3 "" H 8450 3100 60  0000 C CNN
	1    8450 3100
	-1   0    0    -1  
$EndComp
$Comp
L CP C6
U 1 1 571E5DB9
P 8700 3350
F 0 "C6" H 8725 3450 50  0000 L CNN
F 1 "470u 16V" H 8500 3250 50  0000 L CNN
F 2 "Local:C_Radial_D8_L11.5_P3.5" H 8738 3200 30  0001 C CNN
F 3 "" H 8700 3350 60  0000 C CNN
	1    8700 3350
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X02 P3
U 1 1 571E6C57
P 10800 3150
F 0 "P3" H 10800 3300 50  0000 C CNN
F 1 "+3V3" V 10900 3150 50  0000 C CNN
F 2 "Local:Pin_Header_Straight_1x02" H 10800 3150 60  0001 C CNN
F 3 "" H 10800 3150 60  0000 C CNN
	1    10800 3150
	1    0    0    1   
$EndComp
$Comp
L CP C7
U 1 1 571E7B19
P 10200 3350
F 0 "C7" H 10225 3450 50  0000 L CNN
F 1 "1000u 10V" H 9950 3250 50  0000 L CNN
F 2 "Local:C_Radial_D8_L11.5_P3.5" H 10238 3200 30  0001 C CNN
F 3 "" H 10200 3350 60  0000 C CNN
	1    10200 3350
	1    0    0    -1  
$EndComp
Text GLabel 9100 3100 3    60   Input ~ 0
+7VR
Text GLabel 10200 3000 1    60   Input ~ 0
+5V
$Comp
L C C10
U 1 1 571E9299
P 8200 4750
F 0 "C10" H 8225 4850 50  0000 L CNN
F 1 "100n" H 8225 4650 50  0000 L CNN
F 2 "Local:C_0603" H 8238 4600 30  0001 C CNN
F 3 "" H 8200 4750 60  0000 C CNN
	1    8200 4750
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR05
U 1 1 571E9523
P 8200 5300
F 0 "#PWR05" H 8200 5050 50  0001 C CNN
F 1 "GND" H 8200 5150 50  0000 C CNN
F 2 "" H 8200 5300 60  0000 C CNN
F 3 "" H 8200 5300 60  0000 C CNN
	1    8200 5300
	1    0    0    -1  
$EndComp
$Comp
L R R6
U 1 1 571E3810
P 8700 4050
F 0 "R6" V 8780 4050 50  0000 C CNN
F 1 "4K7" V 8700 4050 50  0000 C CNN
F 2 "Local:R_0603" V 8630 4050 30  0001 C CNN
F 3 "" H 8700 4050 30  0000 C CNN
	1    8700 4050
	-1   0    0    -1  
$EndComp
$Comp
L R R10
U 1 1 571EA651
P 9500 5450
F 0 "R10" V 9580 5450 50  0000 C CNN
F 1 "220" V 9500 5450 50  0000 C CNN
F 2 "Local:R_0603" V 9430 5450 30  0001 C CNN
F 3 "" H 9500 5450 30  0000 C CNN
	1    9500 5450
	-1   0    0    1   
$EndComp
Text Notes 900  1950 0    60   ~ 0
/* PCB fuse width selection */\n\nEQ[w]: solve(%pi * (d/2)^2 = h * w, w)[1], solve(I^2 = 80^2 * d^3, d)[3];\nEQ[w], h=0.035, I=3, numer;
$Comp
L THERMISTOR TH1
U 1 1 571F13A5
P 3850 2500
F 0 "TH1" V 3950 2550 50  0000 C CNN
F 1 "NTC 10R" V 3750 2500 50  0000 C BNN
F 2 "Local:NTC_11x5.5_f7.5" H 3850 2500 60  0001 C CNN
F 3 "" H 3850 2500 60  0000 C CNN
	1    3850 2500
	0    -1   -1   0   
$EndComp
$Comp
L CONN_01X01 M3
U 1 1 5743FF30
P 3850 3050
F 0 "M3" H 3850 3150 50  0000 C CNN
F 1 "MOUNT" V 3950 3050 50  0000 C CNN
F 2 "Local:MountingHole_3mm" H 3850 3050 60  0001 C CNN
F 3 "" H 3850 3050 60  0000 C CNN
	1    3850 3050
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR06
U 1 1 5743FF36
P 3850 3350
F 0 "#PWR06" H 3850 3100 50  0001 C CNN
F 1 "GND" H 3850 3200 50  0000 C CNN
F 2 "" H 3850 3350 60  0000 C CNN
F 3 "" H 3850 3350 60  0000 C CNN
	1    3850 3350
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X01 M2
U 1 1 5743FFEF
P 3550 3050
F 0 "M2" H 3550 3150 50  0000 C CNN
F 1 "MOUNT" V 3650 3050 50  0000 C CNN
F 2 "Local:MountingHole_3mm" H 3550 3050 60  0001 C CNN
F 3 "" H 3550 3050 60  0000 C CNN
	1    3550 3050
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR07
U 1 1 5743FFF5
P 3550 3350
F 0 "#PWR07" H 3550 3100 50  0001 C CNN
F 1 "GND" H 3550 3200 50  0000 C CNN
F 2 "" H 3550 3350 60  0000 C CNN
F 3 "" H 3550 3350 60  0000 C CNN
	1    3550 3350
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X01 M1
U 1 1 5743FFFC
P 3250 3050
F 0 "M1" H 3250 3150 50  0000 C CNN
F 1 "MOUNT" V 3350 3050 50  0000 C CNN
F 2 "Local:MountingHole_3mm" H 3250 3050 60  0001 C CNN
F 3 "" H 3250 3050 60  0000 C CNN
	1    3250 3050
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR08
U 1 1 57440002
P 3250 3350
F 0 "#PWR08" H 3250 3100 50  0001 C CNN
F 1 "GND" H 3250 3200 50  0000 C CNN
F 2 "" H 3250 3350 60  0000 C CNN
F 3 "" H 3250 3350 60  0000 C CNN
	1    3250 3350
	1    0    0    -1  
$EndComp
$Comp
L R R11
U 1 1 57467054
P 6100 2950
F 0 "R11" V 6180 2950 50  0000 C CNN
F 1 "M68" V 6100 2950 50  0000 C CNN
F 2 "Local:R_0603" V 6030 2950 30  0001 C CNN
F 3 "" H 6100 2950 30  0000 C CNN
	1    6100 2950
	-1   0    0    1   
$EndComp
$Comp
L R R3
U 1 1 57467419
P 6100 2350
F 0 "R3" V 6180 2350 50  0000 C CNN
F 1 "3M3" V 6100 2350 50  0000 C CNN
F 2 "Local:R_1206" V 6030 2350 30  0001 C CNN
F 3 "" H 6100 2350 30  0000 C CNN
	1    6100 2350
	-1   0    0    1   
$EndComp
Text Notes 3800 1400 0    60   ~ 0
/* Undervoltage threshold resistor selection */\n\nEQ[R[UV]]: R[UV] = U[IN,MIN] / I[UV,TH];\nEQ[I[R[UV],MAX]]: I[R[UV]] = U[IN,MAX] / R[UV];\nEQ[P[R[UV]]]: P[R[UV]] = U[IN,MAX]^2 / R[UV];\nPR[U[IN]]: [I[UV,TH]=25e-6, U[IN,MIN]=100, U[IN,MAX]=350]$\nPR[R[UV]]: EQ[R[UV]], PR[U[IN]];\nPR[I[R[UV],MAX]]: EQ[I[R[UV],MAX]], PR[U[IN]], PR[R[UV]];\nPR[P[R[UV]]]: EQ[P[R[UV]]], PR[U[IN]], PR[R[UV]];
Text Notes 5800 2700 0    60   ~ 0
R[UV]
Text Notes 6250 6350 0    60   ~ 0
/* Output voltage selection */\n\nEQ[U[KA]]: U[KA] = U[REF] * (1 + R[H]/R[L]) + I[REF] * R[H];\nEQ[R[L]]: solve(EQ[U[KA]], R[L])[1];\nPR[IN]: [U[REF]=2.5, I[REF]=2e-6, U[KA]=12.1]$\nPR[R[H]]: R[H]=39e3 /* 39K */;\nEQ[R[L]], PR[IN], PR[R[H]];\nPR[R[L]]: R[L]=10e3+220 /* 10K + 220R */;\nPR[U[KA]]: EQ[U[KA]], U[KA]=U[KA,res], PR[IN], PR[R[H]], PR[R[L]];
Text GLabel 9600 4300 2    60   Input ~ 0
+12VR
Wire Wire Line
	8700 4300 8800 4300
Wire Wire Line
	8700 4200 8700 4600
Wire Wire Line
	9200 4300 9100 4300
Wire Wire Line
	8900 4800 9500 4800
Wire Wire Line
	9500 4700 9500 4900
Wire Wire Line
	9200 4300 9200 4400
Wire Wire Line
	9200 4700 9200 4800
Connection ~ 9200 4800
Connection ~ 9500 4800
Wire Wire Line
	9500 5600 9500 5700
Wire Wire Line
	8700 5000 8700 5300
Wire Wire Line
	8100 4500 8700 4500
Connection ~ 8700 4500
Wire Wire Line
	8100 4300 8200 4300
Wire Wire Line
	8200 4300 8200 4200
Wire Wire Line
	8600 2600 9300 2600
Wire Wire Line
	9000 2500 9000 2700
Wire Wire Line
	8100 2600 8300 2600
Wire Wire Line
	9000 3600 9000 3000
Connection ~ 9000 2600
Wire Wire Line
	7200 2800 7300 2800
Wire Wire Line
	7200 2000 7200 2800
Wire Wire Line
	6100 2100 7300 2100
Wire Wire Line
	7100 3200 7300 3200
Wire Wire Line
	7100 3700 7200 3700
Wire Wire Line
	7200 3700 7200 4500
Wire Wire Line
	7200 4000 7100 4000
Wire Wire Line
	7100 3900 7200 3900
Connection ~ 7200 3900
Wire Wire Line
	7100 3800 7200 3800
Connection ~ 7200 3800
Wire Wire Line
	7200 3600 7100 3600
Wire Wire Line
	7200 3200 7200 3600
Wire Wire Line
	6350 2100 6350 2200
Wire Wire Line
	6700 2100 6700 2200
Wire Wire Line
	6700 2800 6700 2500
Wire Wire Line
	6350 3100 6350 3200
Wire Wire Line
	6350 3200 6800 3200
Wire Wire Line
	6700 3200 6700 3100
Connection ~ 6700 2100
Connection ~ 6700 3200
Connection ~ 7200 3200
Connection ~ 7200 2100
Wire Wire Line
	6300 3600 6200 3600
Wire Wire Line
	6200 3600 6200 4100
Wire Wire Line
	6200 4400 6200 4500
Wire Wire Line
	6200 4500 7500 4500
Wire Wire Line
	6700 4500 6700 4600
Wire Wire Line
	7400 4300 7500 4300
Wire Wire Line
	7400 3400 6100 3400
Wire Wire Line
	6100 3100 6100 4000
Wire Wire Line
	6100 4000 6300 4000
Wire Wire Line
	4700 2500 4800 2500
Wire Wire Line
	4800 2500 4800 2200
Wire Wire Line
	4700 2200 5000 2200
Wire Wire Line
	4700 2800 4800 2800
Wire Wire Line
	4800 2800 4800 3100
Wire Wire Line
	4700 3100 5000 3100
Wire Wire Line
	4400 2800 4300 2800
Wire Wire Line
	4300 2800 4300 2500
Wire Wire Line
	4400 3100 4200 3100
Wire Wire Line
	4200 3100 4200 2200
Wire Wire Line
	4100 2200 4400 2200
Connection ~ 4200 2200
Connection ~ 4300 2500
Wire Wire Line
	3600 2200 3400 2200
Wire Wire Line
	3400 2300 3500 2300
Wire Wire Line
	3500 2300 3500 2500
Wire Wire Line
	5000 2100 5000 2500
Connection ~ 4800 2200
Wire Wire Line
	5000 2800 5000 3200
Connection ~ 4800 3100
Connection ~ 5000 2200
Wire Wire Line
	9200 2700 9300 2700
Wire Wire Line
	9700 3400 9700 3700
Wire Wire Line
	8400 3400 8100 3400
Wire Wire Line
	8200 2900 8100 2900
Wire Wire Line
	7700 2100 7600 2100
Connection ~ 7200 4000
Connection ~ 6700 4500
Connection ~ 7200 4500
Wire Wire Line
	8200 3900 8200 3800
Wire Wire Line
	8100 3800 8700 3800
Wire Wire Line
	7400 3400 7400 3700
Wire Wire Line
	7400 4000 7400 4300
Wire Wire Line
	8100 3100 8300 3100
Wire Wire Line
	8200 3100 8200 2900
Connection ~ 8200 3100
Wire Wire Line
	8600 3100 9300 3100
Wire Wire Line
	8700 3100 8700 3200
Wire Wire Line
	8400 3400 8400 3600
Wire Wire Line
	8700 3500 8700 3600
Wire Wire Line
	8400 3600 10500 3600
Connection ~ 8700 3600
Connection ~ 9700 3600
Connection ~ 9000 3600
Wire Wire Line
	9200 3600 9200 2700
Connection ~ 8700 3100
Wire Wire Line
	10100 3100 10600 3100
Connection ~ 9200 3600
Wire Wire Line
	10200 3600 10200 3500
Wire Wire Line
	10200 3000 10200 3200
Connection ~ 10200 3100
Wire Wire Line
	10500 3600 10500 3200
Wire Wire Line
	10500 3200 10600 3200
Connection ~ 10200 3600
Wire Wire Line
	7700 2100 7700 2200
Wire Wire Line
	8200 4600 8200 4500
Connection ~ 8200 4500
Wire Wire Line
	8200 4900 8200 5300
Wire Wire Line
	8700 3800 8700 3900
Connection ~ 8200 3800
Connection ~ 8700 4300
Wire Wire Line
	4100 2500 4400 2500
Wire Wire Line
	3500 2500 3600 2500
Connection ~ 5000 3100
Wire Wire Line
	6350 2600 6350 2800
Wire Wire Line
	3850 3350 3850 3250
Wire Wire Line
	3550 3350 3550 3250
Wire Wire Line
	3250 3350 3250 3250
Wire Wire Line
	6100 2200 6100 2100
Connection ~ 6350 2100
Connection ~ 6100 3400
Wire Wire Line
	6100 2800 6100 2500
Wire Notes Line
	5950 2350 5900 2350
Wire Notes Line
	5900 2350 5900 2550
Wire Notes Line
	5950 2950 5900 2950
Wire Notes Line
	5900 2950 5900 2750
Wire Wire Line
	9600 4300 9500 4300
Wire Wire Line
	9500 4300 9500 4400
Wire Wire Line
	9500 5200 9500 5300
Text Notes 9600 5300 0    60   ~ 0
R[L]
Wire Notes Line
	9600 5050 9650 5050
Wire Notes Line
	9650 5450 9600 5450
Text Notes 9600 4600 0    60   ~ 0
R[H]
Wire Notes Line
	9650 5050 9650 5150
Wire Notes Line
	9650 5350 9650 5450
Text GLabel 7200 3300 2    60   Input ~ 0
FB_GATE
Text GLabel 6200 3600 0    60   Input ~ 0
BP/M
Text GLabel 6100 4000 0    60   Input ~ 0
EN/UV
Text Notes 750  4500 0    60   ~ 0
/* Power Supply */\n/* V[peak] - max input voltage */\n/* V[min] - min input voltage */\n/* V[bulk,avg,min] - min avg input voltage */\n/* C[bulk] - input rectifier filter capacitor */\n/* I[in,avg] - avg input current */\n/* f[line] - powering line frequency (50Hz) */\n/* eta - coefficient of effeciency */\n\nEQ[P[in]]: P[in] = P[out] / eta;\nEQ[I[in,avg]]: I[in,avg] = P[in] / V[bulk,avg,min];\nEQ[C[bulk]]: C[bulk] = I[in,avg]*(1-1/(%pi*cos(V[min]/V[peak])))/(2*f[line]*(V[peak]-V[min]));\nPR[in]: [f[line]=50, eta=0.75, P[out]=20, V[bulk,avg,min]=310, V[peak]=320, V[min]=250];\nPR[P[in]]: EQ[P[in]], PR[in];\nPR[I[in,avg]]: EQ[I[in,avg]], PR[in], PR[P[in]];\nPR[C[bulk]]: EQ[C[bulk]], PR[in], PR[I[in,avg]], numer;
Text Notes 750  10150 0    60   ~ 0
/* Ferrite core selection */\n/* F - edge coefficient */\n/* A[e] - effective cross-sectional area */\n/* l[e] - effective magnetic path length */\n/* V[e] - effective core volume */\n/* delta - clearance */\n/* B[w] - width of winding */\n/* I[peak] - peak current */\n\nEQ[L]: L = N * Phi / i$\nEQ[LI[peak]^2]: L * I[peak] ^ 2 = B[max] * A[e] * l[e] / (mu[0] * mu[e]);\nEQ[F]: F = 1 + delta / sqrt(A[e]) * log(2 * B[w] / delta);\nEQ[mu[e]]: mu[e] = mu[0] * A[e] / (l[e] / mu[i] + delta / F);\nEQ[A[L]]: A[L] = mu[0] * mu[e] / (l[e] / A[e]);\n\nPR[PC40]: mu[i]=2300$\nPR[EE19]: [A[e]=23e-6, l[e]=39.4e-3]$\nPR[EE25]: [A[e]=51.8e-6, l[e]=57.8e-3]$\nPR[in]: [mu[0]=0.4e-6*%pi, L=1e-3/*1mH*/, delta=0.1e-3, B[w]=8.3e-3, B[max]=300e-3, I[peak]=2]$\nPR[F]: EQ[F], PR[in], PR[EE19], numer;\nPR[mu[e]]: EQ[mu[e]], PR[PC40], PR[EE19], PR[in], PR[F], numer;\nPR[A[L]]: EQ[A[L]], PR[in], PR[mu[e]], PR[EE19], numer;\nPR[LI[peak]^2]: EQ[LI[peak]^2], PR[in], PR[mu[e]], PR[EE19], numer;
Text Notes -3750 7450 0    60   ~ 0
/* Ferrite core selection */\n/* F - edge coefficient */\n/* A[e] - effective cross-sectional area */\n/* l[e] - effective magnetic path length */\n/* V[e] - effective core volume */\n/* delta - magnetic clearance */\n/* B[w] - width of winding */\n/* I[peak] - peak current */\n/* mu[0] - magnetic field constant */\n/* mu[e] - relative effective permeability */\n/* mu[i] - relative initial permeability */\n\nEQ[L]: L = N * Phi / i$\nEQ[LI[peak]^2]: L * I[peak] ^ 2 = B[max] * A[e] * l[e] / (mu[0] * mu[e]);\nEQ[F]: F = 1 + delta / sqrt(A[e]) * log(2 * B[w] / delta);\nEQ[mu[e]]: mu[e] = mu[0] * A[e] / (l[e] / mu[i] + delta / F);\nEQ[A[L]]: A[L] = mu[0] * mu[e] / (l[e] / A[e]);\nEQ[A[L]], EQ[mu[e]]$\n\nPR[N27]: [mu[i]=2000, B[max]=300e-3]$\nPR[PC40]: [mu[i]=2300, B[max]=300e-3]$\nPR[EE19]: [A[e]=23e-6, l[e]=39.4e-3, B[w]=8.3e-3]$\nPR[EE25]: [A[e]=51.8e-6, l[e]=57.8e-3, B[w]=8.3e-3]$\nPR[EE42]: [A[e]=240, l[e]=97, B[w]=26]$\n\nPR[in]: [mu[0]=0.4e-6*%pi, L=1e-3/*1mH*/, delta=2, I[peak]=2]$\n\n/*PR[F]: EQ[F], PR[in], PR[EE19], numer;\nPR[mu[e]]: EQ[mu[e]], PR[PC40], PR[EE19], PR[in], PR[F], numer;\nPR[A[L]]: EQ[A[L]], PR[in], PR[mu[e]], PR[EE19], numer;\nPR[LI[peak]^2]: EQ[LI[peak]^2], PR[in], PR[PC40], PR[mu[e]], PR[EE19], numer;*/\n\nPR[F]: EQ[F], PR[in], PR[EE42], numer;\nPR[mu[e]]: EQ[mu[e]], PR[N27], PR[EE42], PR[in], PR[F], numer;\nPR[A[L]]: EQ[A[L]], PR[in], PR[mu[e]], PR[EE42], numer;\nPR[LI[peak]^2]: EQ[LI[peak]^2], PR[in], PR[N27], PR[mu[e]], PR[EE42], numer;
Text Notes 6950 1400 0    60   ~ 0
/* I[peak] - peak current (A) */\n/* U[r] - reflected voltage (V) */\n/* U[dc,min] - minimum input DC voltage (V) */\nEQ[P[in,max]]: P[in,max] = P[out,max] / eta;\nEQ[D[max]]: D[max] = U[r] / (U[r] + U[dc,min]);\nEQ[I[peak]]: I[peak] = 2 * P[in,max] / (U[dc,min] * D[max]), EQ[D[max]], EQ[P[in,max]];\n\nPR[in]: [U[r]=150, U[dc,min]=90, P[out,max]=20, eta=75e-2];\nEQ[I[peak]], PR[in], numer;
Text Notes 750  7550 0    60   ~ 0
/* U[r] - reflected voltage (V) */\n/* U[dc,min] - minimum input DC voltage (V) */\n/* n - transformation ratio */\n\nEQ[P[in,max]]: P[in,max] = P[out,max] / eta;\nEQ[D[max]]: D[max] = U[r] / (U[r] + U[dc,min]);\nEQ[I[peak]]: I[peak] = 2 * P[in,max] / (U[dc,min] * D[max]);\nEQ[I[pri]]: I[pri] = P[in,max] / U[dc,max];\nEQ[L[pri,max]]: L[pri,max] = U[dc,min] * D[max] / (I[pri] * f[sw]);\nEQ[N[pri]]: N[pri] = L[pri] * I[pri] / (B[max] * A[e]);\nEQ[n]: n = U[r] / (U[out] + U[D]);\n\nPR[in]: [U[r]=150, U[dc,min]=90, U[dc,max]=320, P[out,max]=20, eta=75e-2, f[sw]=125e3]$\nPR[out]: [U[out]=12, U[D]=0.8];\nPR[out2]: [U[out]=5, U[D]=0.8];\nPR[PC40]: [mu[i]=2300, B[max]=300e-3]$\nPR[EE19]: [A[e]=23e-6, l[e]=39.4e-3]$\n\nPR[P[in,max]]: EQ[P[in,max]], PR[in], numer;\nPR[I[pri]]: EQ[I[pri]], PR[in], PR[P[in,max]];\nPR[D[max]]: EQ[D[max]], PR[in], numer;\nPR[I[peak]]: EQ[I[peak]], PR[in], PR[P[in,max]], PR[D[max]], numer;\nPR[L[pri,max]]: EQ[L[pri,max]], PR[in], PR[D[max]], PR[I[pri]], numer;\nPR[L[pri]]: L[pri]=1e-3;\nEQ[N[pri]], PR[I[pri]], PR[L[pri]], PR[PC40], PR[EE19];\nPR[N[pri]]: N[pri]=68;\nPR[n]: EQ[n], PR[in], PR[out];\nPR[n2]: EQ[n], PR[in], PR[out2];
Text Notes 7200 3100 0    60   ~ 0
68T\n29AWG\n(mm3)
Text Notes 8100 2050 0    60   ~ 0
EQ: N[sec] = N[pri] / n[tr];\nPR: [N[pri]=68, n[12]=7.5, n[5.3]=17, n[7.8]=11.5];\nEQ, n[tr]=n[12], N[sec]=N[12], PR;\nEQ, n[tr]=n[5.3], N[sec]=N[5.3], PR;\nEQ, n[tr]=n[7.8], N[sec]=N[7.8], PR;
Text Notes 8000 2900 0    60   ~ 0
4T\n24AWG\n(mm5)
Text Notes 8000 3400 0    60   ~ 0
5T\n24AWG\n(mm5)
Text Notes 11700 4250 0    60   ~ 0
/* Ferrite core selection */\n/* A[e] - effective cross-sectional area (m^2) */\n/* l[e] - effective magnetic path length (m) */\n/* V[e] - effective core volume (m^3) */\n/* l[g] - ferrite core gap length (m) */\n/* B[max] - magnet flux density (Gauss) */\n/* I[peak] - peak current (A) */\n/* N - number of turns */\n/* mu[0] - magnetic field constant (H/m) */\n/* mu[e] - relative effective permeability (H/m) */\n/* mu[i] - relative initial permeability (H/m) */\n\nEQ[mu[e]]: mu[e] = B[max] * l[e] / (mu[0] * N * I[peak]);\nEQ[l[g]]: l[g] = l[e] * (1/mu[e] - 1/mu[i]);\n\nPR[in]: [mu[0]=0.4e-6*%pi, L=1e-3/*1mH*/, I[peak]=1, N=62]$\nPR[N27]: [mu[i]=2000, B[max]=300e-3]$\nPR[PC40]: [mu[i]=2300, B[max]=300e-3]$\nPR[EE19]: [A[e]=23e-6, l[e]=39.4e-3]$\nPR[EE25]: [A[e]=51.8e-6, l[e]=57.8e-3]$\n\nPR[mu[e]]: EQ[mu[e]], PR[in], PR[PC40], PR[EE19], numer;\nPR[l[g]]: EQ[l[g]], PR[PC40], PR[EE19], PR[mu[e]], numer;
$Comp
L LED D11
U 1 1 5747EBF3
P 10700 4800
F 0 "D11" H 10700 4900 50  0000 C CNN
F 1 "RED/YELLOW" H 10700 4700 50  0000 C CNN
F 2 "Local:LED_0603" H 10700 4800 60  0001 C CNN
F 3 "" H 10700 4800 60  0000 C CNN
	1    10700 4800
	0    -1   -1   0   
$EndComp
Text GLabel 10700 4500 1    60   Input ~ 0
+5V
$Comp
L R R13
U 1 1 5747ED20
P 10700 5250
F 0 "R13" V 10780 5250 50  0000 C CNN
F 1 "150" V 10700 5250 50  0000 C CNN
F 2 "Local:R_0603" V 10630 5250 30  0001 C CNN
F 3 "" H 10700 5250 30  0000 C CNN
	1    10700 5250
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR09
U 1 1 5747EDE7
P 10700 5500
F 0 "#PWR09" H 10700 5250 50  0001 C CNN
F 1 "GND" H 10700 5350 50  0000 C CNN
F 2 "" H 10700 5500 60  0000 C CNN
F 3 "" H 10700 5500 60  0000 C CNN
	1    10700 5500
	1    0    0    -1  
$EndComp
Wire Wire Line
	10700 4500 10700 4600
Wire Wire Line
	10700 5000 10700 5100
Wire Wire Line
	10700 5400 10700 5500
$Comp
L LED D10
U 1 1 5747F308
P 10300 4800
F 0 "D10" H 10300 4900 50  0000 C CNN
F 1 "GREEN/BLUE" H 10300 4700 50  0000 C CNN
F 2 "Local:LED_0603" H 10300 4800 60  0001 C CNN
F 3 "" H 10300 4800 60  0000 C CNN
	1    10300 4800
	0    -1   -1   0   
$EndComp
$Comp
L R R12
U 1 1 5747F30F
P 10300 5250
F 0 "R12" V 10380 5250 50  0000 C CNN
F 1 "470" V 10300 5250 50  0000 C CNN
F 2 "Local:R_0603" V 10230 5250 30  0001 C CNN
F 3 "" H 10300 5250 30  0000 C CNN
	1    10300 5250
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR010
U 1 1 5747F315
P 10300 5500
F 0 "#PWR010" H 10300 5250 50  0001 C CNN
F 1 "GND" H 10300 5350 50  0000 C CNN
F 2 "" H 10300 5500 60  0000 C CNN
F 3 "" H 10300 5500 60  0000 C CNN
	1    10300 5500
	1    0    0    -1  
$EndComp
Wire Wire Line
	10300 4500 10300 4600
Wire Wire Line
	10300 5000 10300 5100
Wire Wire Line
	10300 5400 10300 5500
Text GLabel 10300 4500 1    60   Input ~ 0
+12VR
$Comp
L MC78M05CDT U1
U 1 1 576CF961
P 9700 3150
F 0 "U1" H 9500 3350 50  0000 C CNN
F 1 "MC78M05CDT" H 9650 3350 50  0000 L CNN
F 2 "Local:TO-252-2Lead" H 9700 3250 50  0001 C CIN
F 3 "" H 9700 3150 50  0000 C CNN
	1    9700 3150
	1    0    0    -1  
$EndComp
$EndSCHEMATC
